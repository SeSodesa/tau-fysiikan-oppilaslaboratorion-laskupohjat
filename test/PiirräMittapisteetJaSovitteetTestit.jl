module PiirräMittapisteetJaSovitteetTestit

using Test
using TAUFysOplabLaskupohjat
using LsqFit

const KUVAN_ETULIITE = "test_piirrä_mittapisteet_ja_sovitteet"
const SUORAN_PARAMETRIEN_ALKUARVIOT = [1.0, 1.0]
const käyrärangi = 1 : 10
@. suora(x, p) = p[1] * x + p[2]
@. eksponentti(x, p) = p[1] * exp(p[2] * x)

@testset verbose=true "piirrä_mittapisteet_ja_sovitteet" begin

    @testset verbose=true "matriisimetodi" begin
        xmat = [i for i ∈ käyrärangi, _ ∈ käyrärangi]
        ymat = [i + j for j ∈ käyrärangi, i ∈ 0 : 9]

        sovitteet = [
            curve_fit(suora, xmat[:,i], ymat[:,i], SUORAN_PARAMETRIEN_ALKUARVIOT)
            for i ∈ käyrärangi
        ]

        @test piirrä_mittapisteet_ja_sovitteet(
            KUVAN_ETULIITE * "matriisi.pdf",
            xmat,
            ymat,
            [ fit.param for fit ∈ sovitteet ],
            [ suora for _ ∈ käyrärangi ],
            "vaakaotsikko",
            "pystyotsikko",
            [string("pistejoukko ", i) for i ∈ käyrärangi],
            selitelaatikon_sijainti = :topleft
        ) === nothing
    end

    @testset verbose=true "vektorimetodi" begin

        xvecs = [ [ i for i ∈ käyrärangi ] for _ ∈ käyrärangi ]
        yvecs = [ [ i + j for j ∈ käyrärangi ] for i ∈ 0 : 9 ]

        sovitteet = [
            curve_fit(suora, xvecs[i], yvecs[i], SUORAN_PARAMETRIEN_ALKUARVIOT)
            for i ∈ käyrärangi
        ]

        @test piirrä_mittapisteet_ja_sovitteet(
            KUVAN_ETULIITE * "vektori.pdf",
            xvecs,
            yvecs,
            [ fit.param for fit ∈ sovitteet ],
            [ suora for _ ∈ käyrärangi ],
            "vaakaotsikko",
            "pystyotsikko",
            [string("pistejoukko ", i) for i ∈ käyrärangi],
            selitelaatikon_sijainti = :topleft
        ) === nothing
    end

    @testset verbose=true "vektorimetodi ja virheet" begin

        xvecs = [ [ i for i ∈ käyrärangi ] for _ ∈ käyrärangi ]
        yvecs = [ [ i + j for j ∈ käyrärangi ] for i ∈ 0 : 9 ]
        virheet = Dict(1 => [1,3])

        sovitteet = [
            curve_fit(suora, xvecs[i], yvecs[i], SUORAN_PARAMETRIEN_ALKUARVIOT)
            for i ∈ käyrärangi
        ]

        @test piirrä_mittapisteet_ja_sovitteet(
            KUVAN_ETULIITE * "vektoritjavirheet.pdf",
            xvecs,
            yvecs,
            [ fit.param for fit ∈ sovitteet ],
            [ suora for _ ∈ käyrärangi ],
            "vaakaotsikko",
            "pystyotsikko",
            [string("pistejoukko ", i) for i ∈ käyrärangi],
            selitelaatikon_sijainti = :topleft,
            virheindeksit = virheet
        ) === nothing
    end

    @testset verbose=true "yksittäinen eksponentti" begin

        xdata = [ i for i in 1 : 10 ]
        ydata = exp.(xdata)

        sovite = curve_fit(eksponentti, xdata, ydata, [0.1, 0.1])

        @test piirrä_mittapisteet_ja_sovitteet(
            KUVAN_ETULIITE * "yksittäinen_eksponentti.pdf",
            xdata,
            ydata,
            sovite.param,
            eksponentti,
            "vaakaotsikko",
            "pystyotsikko",
            "pistejoukko",
        ) === nothing
    end
end

end
