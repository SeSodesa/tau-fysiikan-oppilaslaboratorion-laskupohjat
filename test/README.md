# Testit

Sisältää pakettiin TAUFysOplabLaskupohjat liittyviä yksikkötestejä. Testit voi
ajaa manuaaliesti Julian paketinhallinnan Pkg kautta ajamalla seuraavat komennot
projektin juuressa:

```julia
julia> ]
  pkg> activate .
  pkg> test
```

Pkg ajaa tällöin tiedoston `runtests.jl` Julian läpi.
