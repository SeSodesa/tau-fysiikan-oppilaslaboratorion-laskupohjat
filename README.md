# TAU Fysiikan oppilaslaboratorion laskupohjat

Tämä repositorio sisältää kielellä [Julia][julia] kirjoitettuja laskupohjia
Tampereen yliopiston fysiikan oppilaslaboratorion töiden tulosten laskemista
varten. Tarkoituksena on, että kansiosta `laskupohjat` löytyviin työkohtaisiin
pohjiin täytetään puuttuvat osat tulosten muodostamista ajatellen.

Tämän on ensinnäkin tarkoitus tutustuttaa töiden tekijät
ohjelmoinnin perusteisiin sen teoriaan liikaa uppoutumatta,
ja toisaalta tutkimusryhmien käyttämien työkalujen
verrannaisten käyttöön. Julia on käytännössä robustimpi
avoimen lähdekoodin versio Matlabista ja näin paljon
taulukkolaskentaohjelmistoja ilmaisuvoimaisempi. Tällä kurssilla
tarvitsee kuitenkin tutustua vain kielen perusominaisuuksiin.


## Sisällysluettelo

[[_TOC_]]

## Tarvitset…

… [Julia-kääntäjän][julia-compiler] (versio ≥ 1.7.0), sekä
komentotulkin kuten [PowerShell][powershell-getting-started],
joka on jokaisella Windows 10 -koneella valmiiksi asennettuna.
Vielä modernimpi vaihtoehto on [Windows Terminal][windows-terminal].
Linux- ja Mac-käyttäjät pärjäävät hyvin omilla oletusterminaaleillaan.

Tarvitset myös tekstieditorin, jolla muokata skriptitiedostoja.
Tällä hetkellä paras vaihtoehto Julia-tiedostojen muokkaamiseen
on [VS Code][vs-code] tai [VS Codium][vs-codium],
kunhan niihin on asennettu niiden [Julia-lisäosa][julia-vs-addon].
Myös [Vimiin][vim] on tarjolla oma Julia-lisäosa, [julia-vim][julia-vim].


## Tiedostojen lataaminen omalle koneelle

Tämän repositorion tiedostot saa käyttöönsä joko kloonaamalla ne omalle
koneelle versionhallintaohjelmiston Git avulla, komennolla

    git clone ⟨tämän repositorion URL⟩.git ⟨kohdekansio omalla koneella⟩

tai painamalla tämän sivun oikeasta ylälaidasta painiketta <kbd>Download</kbd>
tai <kbd>⌊↓⌋</kbd> ja purkamalla ladatun zip-paketin haluamaansa kansioon.
Projektin *juuri* on se kansio, jossa tämä tiedosto `README.md` sijaitsee.

## Julian käyttö

Asenna Julia koneellesi sivun https://julialang.org/downloads/platform/
ohjeiden mukaisesti:

* [Windows][windows-julia-installation]
* [Mac][mac-julia-installation]
* [Linux][linux-julia-installation]

Käynnistä sitten käyttöjärjestelmäsi komentotulkki `shell` ja navigoi tämän
projektin juureen. Linuxilla ja Macillä tämä tapahtuu komennolla

    cd polku/projektin/kansioon # Linux ja MacOS

Windowsilla kansioselaimessa näppäinyhdistelmä <kbd>Shift</kbd> +
<kbd>Hiiren oikea</kbd> → "Avaa PowerShell tähän" tai itse PowerShellissä
komento

    cd polku\projektin\kansioon # Windows

ajaa saman asian. Viimeisenä aja terminaalissa malliselostuksen laskentapohja
komennolla

    julia laskupohjat/malliselostus.jl

Jos ajat jotakin kansion `laskupohjat` laskuskriptiä ensimmäistä kertaa tiedostojen
koneellesi lataamisen jälkeen, Julia asentaa ensin projektin riippuvuudet
koneellesi. Voit tässä välissä käydä nopeasti kahvilla, sillä varsinkin
piirtokirjaston [Plots.jl][plots.jl] lataamisessa ja kääntämisessä menee hetki.

Ruudulle pitäisi kirjastojen asentamisen jälkeen tulostua malliselostuksen
laskujen tulokset ja kansioon `kuvat` pitäisi ilmestyä kuva
`malliselostuksen_mittausdata.pdf`. Muut skriptit ajetaan samalla tavalla,
vaihtamalla merkkijono `malliselostus.jl` halutun kansiosta `laskupohjat`
löytyvän tiedoston nimeksi.

### Jos komento `julia` ei toimi komentotulkissa…

… Julian asentamisen jälkeen, et ole kertonut käyttöjärjestelmällesi mistä se
löytyy. Tämä tapahtuu lisäämällä kääntäjän sijainti ympäristömuuttujaan `PATH`,
joka pitää kirjaa kaikista käynnistettävien ohjelmistojen sijainneista.

Katso osoitteesta
<https://julialang.org/downloads/platform/#adding_julia_to_path_on_windows_10>,
miten Julian lisääminen polkulistaan `PATH` tapahtuu.

### Skriptien ajaminen Julian omassa komentotulkissa

Jos Julia-ohjelmat ajaa käyttäen käyttöjärjestelmän omaa komentotulkkia, joutuu
Julia kääntämään ohjelman funktiot joka kerta uudestaan, sillä Julia sammuu
näin ajojen välissä ja unohtaa tekemänsä asiat. Tämä saataa vaatia jokin verran
odottelua Julian käyttäjältä. Tätä varten on suositeltavaa käyttää Juliaa sen
oman komentotulkin kautta.

Julian oman komentotulkin käynnistääksesi, aja komento `julia` ilman
ohjelmatiedostoa parametrina, pitäen mielessä Windowsin kansioerottimen olevan
kenoviiva `\` vinoviivan `/` sijaan:

    shell> julia
               _
       _       _ _(_)_     |  Documentation: https://docs.julialang.org
      (_)     | (_) (_)    |
       _ _   _| |_  __ _   |  Type "?" for help, "]?" for Pkg help.
      | | | | | | |/ _` |  |
      | | |_| | | | (_| |  |  Version 1.6.1 (2021-04-23)
     _/ |\__'_|_|_|\__'_|  |  Official https://julialang.org/ release
    |__/                   |

    julia> include("laskupohjat/malliselostus.jl")

Tässä listauksessa `shell` viittaa käyttöjärjestelmän komentotulkkiin ja
`julia` Julian komentotulkkiin. Julian funktio `include` sisällyttää sille
annetun ohjelmatiedoston nykyiseen istuntoon, mikä vastaa ohjelmatiedoston
ajamista. Komennot ajettuasi ruudulle pitäisi ilmestyä
[malliselostuksen][malliselostus] laskujen tulokset ja kansioon `kuvat` pitäisi
tulostua mittaustuloksista piirretty kuva
`laskupohjat/malliselostuksen_mittausdata.pdf`.

Jos jokin komennoista ei toimi, varmista Julian käynnistettyäsi, että olet
oikeassa työhakemistossa eli tämän projektin juuressa, ajamalla komento

    julia> pwd() # pwd eli print working directory

Tämä tulostaa nykyisen työhakemiston polun. Jos tulostettu polku ei vastaa
projektin juurta, voit navigoida projektin hakemistoon myös Julian sisällä
komennolla

```julia
julia> cd("polku/tähän/hakemistoon") # cd eli change directory
```

Laskuskriptit täydennetään avaamalla ne sopivassa tekstieditorissa. Julian
komentotulkista poistutaan kirjoittamalla `exit()`.

## Laskujen aloittaminen

Tee heti ensimmäisenä kuten yllä ohjeistettiin ja aja
[malliselostuksen][malliselostus] laskupohja `laskupohjat/malliselostus.jl`
Julian läpi komennolla

    julia polku/tiedostoon/laskupohjat/malliselostus.jl

Skripti lataa Julian virallisista repositorioista kirjastot
[LsqFit.jl][lsqfit.jl] ja [Plots.jl][plots.jl] koneellesi, jonka jälkeen se
tekee itse laskut ja kuvien muodostamiset. Sama ympäristön aktivointi on
sijoitettu myös muiden skriptien alkuun, mutta malliselostus testaa myös
kirjaston [Plots.jl][plots.jl] toimivuuden.

[Malliselostuksen][malliselostus] laskupohja on myös syytä avata
tekstieditorissa, sillä siellä on esimerkkejä koskien laskujen suorittamista.
Näitä ei ole tarkoitus ymmärtää tässä vaiheessa syvemmin teorian tasolla, vaan
niitä on lähinnä tarkoitus soveltaa muissa laskupohjissa. Kaikki työkohtaiset
laskupohjat löytyvät kansiosta `laskupohjat` vastaavan työn nimellä.

## Ohjelmoinnin perusteita

Tässä esitetään lyhyesti ohjelmoinnin perusteita. Kunhan nämä asiat ovat edes
jollakin tasolla hallussa, ei laskupohjien muodostamisessa pitäisi tulla
vastaan suurempia ongelmia.

### Muuttujat

Muuttujien käyttö mahdollistaa tietokoneen muistissa olevien arvojen kuvaavan
nimeämisen ja ehkä vielä tärkeämmin uudelleenkäyttämisen. Käytetään esimerkkinä
organisaation palkkataulukkoa, jossa ylemmät palkat perustuvat aina tiettyyn
pohjapalkkaan:

    pohjapalkka = 1200
    peruspalkka = 3//2 * pohjapalkka
    johtajan_palkka = 400 * peruspalkka

Tässä *sijoitusoperaattori* `=` tallentaa sen vasemmalla puolella olevaan
muuttujaan operaatorin oikealla puolella olevan arvon. Nyt pelkkä pohjapalkan
muuttaminen riittää muuttamaan sekä peruspalkkaa, että johtajan palkkaa, sillä
johtajan palkka riippuu peruspalkasta, joka edelleen riippuu pohjapalkasta.


### Alirutiinit ja funktiot

Siinä missä muuttujat mahdollistavat tiettyjen arvojen nimeämisen kuvaavasti,
mahdollistavat *alirutiinit* ja/tai *funktiot* toiminnallisuuden kuvaavan
nimeämisen ja toistamisen eri lähtöarvoilla.

```julia
"""
Funktio, joka laskee nopeuden annetun matkan `x` ja ajan `t` perusteella.
Funktion viimeinen rivi joka ei lopu puolipisteeseen ; palautetaan funktiosta
ja voidaan ottaa kiinni sijoitusoperaattorin `=` avulla funktion kutsupaikassa.
"""
function nopeus(x, t)
    x / t
end

# Lähtöarvot
matka_1 = 5; aika_1 = 2
matka_2 = 1; aika_2 = 3

# Lasketaan samalla funktiolla eri nopeudet ja tallennetaan ne kuvaavilla
# nimillä varustetuihin muuttujiin.
nopeus_1 = nopeus(matka_1, aika_1)
nopeus_2 = nopeus(matka_2, aika_2)
```

Tässä eri lähtöarvoilla saadaan laskettua kaksi eri nopeutta samalla funktiolla
`nopeus`. Funktio on siis *automaatti*, joka ottaa sisäänsä asioita ja sylkee
ulos laskennan tuloksen. Kaikki alirutiinit eivät kuitenkaan ole funktioita:

```julia
"""
Kertoo onko annettu `nopeus` suurempi kuin 2 yksikköä.
Funktio kutsuu Julian mukana tulevaa valmista funktiota `println`,
joka tulostaa sille annettuja asioita näytölle.
"""
function onko_nopeus_yli_2_yksikköä(nopeus)
    if nopeus > 2
        println("Nopeus $nopeus on yli 2 yksikköä.")
    else
        println("Nopeus $nopeus ei ole yli 2 yksikköä.")
    end
end

onko_nopeus_yli_2_yksikköä(nopeus_1)
onko_nopeus_yli_2_yksikköä(nopeus_2)
```

Tämä alirutiini ei kuitenkaan palauta mitään, tai tarkemmin sanottuna sen
paluuarvo on Juliassa `nothing`. Periaatteessa kaikki alirutiinit ovat siis
Juliassa funktioita, mutta näin ei ole kaikissa ohjelmointikielissä. Alla on
esimerkkejä tämän kurssin kannalta hyödyllisten funktioiden määrittelyistä ja
käyttämisestä, mutta sitä ennen…

### Skalaarit, listat ja matriisit

Kuten kaikki muutkin numeeriseen laskentaan erikoistuneet kielet, Julia tarjoaa
helpohkon käyttöliittymän näiden rakenteiden kanssa toimimiseen. Näiden
toimintaperiaatteet on hyvä tiedostaa jo tässä vaiheessa.

#### Peruslaskutoimituksia

*Skalaarit* ovat yksinkertaisesti asioita kuten numeroita, joilla voi
*skaalata* vektoreita kuten listoja ja tietyn kokoisia matriiseja eli
taulukoita:

```julia
# Luodaan skalaari sekä pari erimuotoista listaa ja julistetaan näiden tyypit.
skalaari    = 5         ::Number
sarakelista = [1, 2, 3] ::Vector
rivilista   = [1  2  3] ::Matrix
matriisi    = [
    1   2   3;
    4   5   6;
    7   8   9]          ::Matrix
# Skaalataan vektoreita skalaarilla
skaalattu_sarakelista = skalaari * sarakelista
skaalattu_rivilista   = skalaari * rivilista
skaalattu_matriisi    = skalaari * matriisi
```

Kun nämä operaatiot on suoritettu, Julian tiedossa ovat seuraavat arvot:

```julia
julia> skaalattu_sarakelista = skalaari * sarakelista
3-element Vector{Int64}:
  5
 10
 15

julia> skaalattu_rivilista = skalaari * rivilista
1×3 Matrix{Int64}:
 5  10  15

julia> skaalattu_matriisi = skalaari * matriisi
3×3 Matrix{Int64}:
  5  10  15
 20  25  30
 35  40  45
```

Listoja ja matriiseja voi myös summata yhteen, jos niiden koot vastaavat
toisiaan:

```julia
julia> sarakelista + sarakelista
3-element Vector{Int64}:
 2
 4
 6

julia> sarakelista + rivilista
ERROR: DimensionMismatch("dimensions must match: …")

julia> matriisi + skaalattu_matriisi
3×3 Matrix{Int64}:
  6  12  18
 24  30  36
 42  48  54
```

Jos vastaan tulee tämän tyyppisiä virheitä, listojen pituudet tai matriisien
rivien ja sarakkeiden määrät eivät siis vastaa toisiaan.

#### Listojen ja matriisien indeksointi

Listoista ja matriiseista voi myös hakea yksittäisiä arvoja tai arvovälejä
*karteesisella indeksoinnilla*. Käytetään esimerkissä yllä määriteltyjä listoja
ja matriiseja:

```julia
rivilistan_ensimmäinen_alkio     = rivilista[begin] # tai rivilista[1]
rivilistan_viimeinen_alkio       = rivilista[end]
rivilistan_2_ensimmäistä_alkiota = rivilista[1:2]
rivilistan_2_viimeistä_alkiota   = rivilista[end-1:end]
rivilistan_kaikki_alkiot         = rivilista[:]
```

Huomaa matriisien *sarakemajoranttius* eli alkoiden järjestyminen sarakkeet
ensin:

```julia
julia> matriisi = [1   2   3; 4   5   6; 7   8   9]
3×3 Matrix{Int64}:
 1  2  3
 4  5  6
 7  8  9

julia> matriisi[2]
4

julia> matriisi[4:6]
3-element Vector{Int64}:
 2
 5
 8

julia> matriisi[begin:end]
9-element Vector{Int64}:
 1
 4
 7
 2
 5
 8
 3
 6
 9
```

#### Vektoreiden yhteen liittäminen

Yllä esitetyn indeksoinnin avulla voi liittää vektoreita kätevästi yhteen,
kunhan niiden koot vastaavat toisiaan sopivasti:

```julia
julia> perkäkkäinen_sarakelista = [sarakelista; sarakelista]
6-element Vector{Int64}:
 1
 2
 3
 1
 2
 3

julia> peräkkäinen_rivilista = [rivilista rivilista]
1×6 Matrix{Int64}:
 1  2  3  1  2  3
```

Tällä tekniikalla voi tehdä kaikenlaista kätevää kuten jättää listoista
karkeita mittausvirheitä pois:

```julia
julia> peräkkäisen_sarakelistan_alku_ja_loppu = [
       peräkkäinen_sarakelista[1:2]; peräkkäinen_sarakelista[end-1:end]]
4-element Vector{Int64}:
 1
 2
 2
 3
```

Tästä on esimerkki muun muassa malliselostuksen laskentapohjassa
`laskupohjat/malliselostus.jl`. Tämä ei ole kaikista tehokkain tapa valita
listoista alkioita muistinkulutuksen tai laskennan nopeuden kannalta, mutta
tekee tehtävänsä tällä kurssilla.


## Sovitteiden tekemisestä

Lähes kaikissa kurssin töissä tarvitsee tehdä jonkinlainen *sovite* joko
suoraan mittauksesta saatuun dataan tai sitten mittausdatasta johdettuun
dataan. Tämä on suositeltavaa tehdä kirjaston [LsqFit.jl][lsqfit.jl] avulla ja
tapahtuu likimain seuraavasti:

```julia
# Ladataan kirjasto tiedoston alussa
using LsqFit

"""
Tämä malli annetaan sovitteen muodostavalle funktiolle LsqFit.curve_fit.
Se kertoo sovitteen muodostavalle funktiolle, minkä muotoinen käyrä dataan
pitäisi sovittaa.

x ↦ vaaka-akselin arvot
p ↦ sovitteen parametrit. Suoralla esim. kulmakerroin p[1] ja vakiotermi p[2].
"""
function suoran_mallifunktio(x, p)
    p[1] .* x .+ p[2]
end

# Luodaan vaaka- ja pystydata sekä alkuarvot parametreille
vaakadata = [1, 2, 3]
pystydata = [4, 5, 6]
alkuarvot_liukulukuina = [1.5, 2.8]

# Muodostetaan mallifunktion avulla suoran sovite.
suoran_sovite = LsqFit.curve_fit(
    suoran_mallifunktio,
    vaakadata,
    pystydata,
    alkuarvot_liukulukuina)

# Otetaan sovitteesta ulos parametrit
kulmakerroin = suoran_sovite.param[1]
vakiotermi   = suoran_sovite.param[2]

# Lasketaan myös standardivirheet sovitteesta
parametrien_virheet = LsqFit.stderror(suoran_sovite)
Δkulmakerroin = parametrien_virheet[1]
Δvakiotermi   = parametrien_virheet[2]
```

Nyt parametreja voi käyttää esimerkiksi sovitteen piirtämiseen. Tästä lisää
seuraavassa kohdassa.


## Kuvien piirtämisestä

Standarditapa muodostaa kuvia Juliassa on kirjaston [Plots.jl][plots.jl]
avulla. Yleisesti kuvien muodostaminen tapahtuu likimain seuraavasti:

```julia
# Ladataan kirjasto Plots.jl tiedoston alussa
using Plots

# Muodostetaan jossakin välissä piirrettävä data
vaakadata = …
pystydata = …

# Luodaan kuva
kuva = Plots.plot(⟨kuvan asetukset kuten otsikot ja selitelaatikon sijainti⟩)

# Sijoitetaan kuvaan kuvaajia. Huomaa huutomerkit funktioiden nimissä.
viivakuvaaja  = plot!(kuva, vaakadata, pystydata, label="teksti selitelaatikossa")
pylväskuvaaja = bar!(kuva, vaakadata, pystydata, label="teksti selitelaatikossa")
pistekuvaaja  = scatter!(kuva, vaakadata, pystydata, label="teksti selitelaatikossa")

# Muodostetaan kuvan sijantia vastaava merkkijono alustariippumattomasti.
# Makro @__DIR__ muuttuu ajon aikana ohjelmatiedoston sisältäväksi kansioksi.
# Tiedostopääte määrittää tiedostomuodon, joka on tässä PDF.
kuvan_polku = joinpath(@__DIR__, "kuvatiedoston_nimi.pdf")

# Tallennetaan kuva yllä luotuun polkuun
Plots.savefig(kuva, kuvan_polku)
```

Nämä askeleet kannattaa yleensä sijoittaa omaan funktioonsa, varsinkin jos
työssä on tarve piirtää useampi samanmuotoinen kuvaaja hieman eri
parametreilla:

```julia
# Ladataan kirjasto Plots.jl tiedoston alussa
using Plots

"""
Piirtää mittapisteet ja suoran sovitteen annetun vaaka- ja pystydatan
perusteella. Annotaatiot `argumentti::Tyyppi` kertovat, minkä tyyppisiä arvoja
funktion argumenttien täytyy olla.
"""
function piirrä_suoran_sovite(
    vaakadata::Vector,
    pystydata::Vector,
    kulmakerroin::Number,
    vakiotermi::Number,
    x_otsikko::String,
    y_otsikko::String,
    datan_nimi_selitelaatikossa::String,
    kuvan_nimi_tiedostopäätteineen::String
)
    # Luodaan kuva asetuksineen
    kuva = Plots.plot(
        xlabel=x_otsikko,
        ylabel=y_otsikko,
        legend=:bottomright)

    # Sijoitetaan mittapisteet kuvaan. Huomaa huutomerkki funktion nimessä.
    pistekuvaaja = scatter!(
        kuva,
        vaakadata,
        pystydata,
        label=datan_nimi_selitelaatikossa)

    # Sitten muodostetaan sovite.
    # Se tarvitsee omat x-arvot, alkaen mittaudatan vaaka-arvojen ensimmäisestä
    # alkiosta sen viimeiseen alkioon 0.1 yksikön välein.
    sovitteen_vaakadata = vaakadata[begin] : 0.1 : vaakadata[end]

    # Sitten sovitteen vaakadatasta lasketaan mallifunktion avulla sen
    # pystyarvot
    sovitteen_pystydata = suoran_mallifunktio(
        sovitteen_vaakadata,
        [kulmakerroin, vakiotermi])

    # Piirretään sovitteen pisteet samaan kuvaan kuin mittapisteet
    # murtoviivoina funktion plot! avulla.
    sovitteen_kuvaaja = plot!(
        kuva,
        sovitteen_vaakadata,
        sovitteen_pystydata,
        label="Suora($datan_nimi_selitelaatikossa)")

    # Tallennetaan kuva tämän tiedoston viereen
    kuvan_polku = joinpath(@__DIR__, kuvan_nimi_tiedostopäätteineen)
    Plots.savefig(kuva, kuvan_polku)
end
```

Näin kuvan muodostuminen eri parametreilla helpottuu. Ohjeet mainittujen kuvien
asetuksien käyttöön löytyvät [täältä][plots.jl attributes].


## Juliaan liittyviä kummallisuuksia: JIT-kääntäminen

Kuten monet numeeriseen laskentaan tarkoitetut kielet tai kirjastot, kuten
Matlab tai Pythonin Numba-kirjasto, Julia on niin kutsuttu JIT eli *Just In
Time* -käännettävä kieli<sup>[1](#alaviite-jaot-kääntäminen)</sup>. Tämä
tarkoittaa sitä, että Julia-kääntäjä kääntää kohtaamansa funkiot ja alirutiinit
vasta, kun se kohtaa ne ensimmäistä kertaa ohjelman ajon aikana. Tämä edelleen
johtaa siihen, että ohjelmat jotka koostuvat monesta yksinkertaisesta
alirutiinista ovat hitaita suorittaa, ja tällaisen ohjelman ajo saattaa
vaikuttaa kovinkin pätkivältä Julian pysähtyessä kääntämään funktioita ennen
niiden suorittamista.

Tämä hitaus ilmenee erityisesti kuvien piirtämisen yhteydessä, sillä
kuvapiirtokirjastot kuten [GR][gr], jota kirjasto [Plots.jl][plots.jl] käyttää
oletuksena taustalla, ajavat yleensä monta pientä funktiota kuvia piirtääkseen.
Julian hyödyt tulevatkin ilmi vasta sovelluksissa, joissa jotakin asiaa
tarvitsee toistaa monta kertaa yhden ajon aikana, mikä on tyypillistä
esimerkiksi simulaatioille laskennallisessa fysiikassa.

Yksinkertaisen syntaksinsa puolesta Julia soveltuu kuitenkin hyvin tämän
kurssin laskujen kirjoittamiseen.

<a name="alaviite-jaot-kääntäminen">¹</a>
Tarkemmin sanottuna Julia kuuluu on [JAOT][jaot-compilation]-käännettävien
kielten perheeseen.

<!-- Linkkejä -->

[julia]: https://julialang.org/
[julia-compiler]: https://julialang.org/downloads/
[powershell-getting-started]: https://docs.microsoft.com/en-us/powershell/scripting/learn/ps101/01-getting-started?view=powershell-7.1#where-do-i-find-powershell
[windows-terminal]: https://www.microsoft.com/fi-fi/p/windows-terminal/9n0dx20hk701
[vs-code]: https://code.visualstudio.com/
[vs-codium]: https://vscodium.com/
[julia-vs-addon]: https://marketplace.visualstudio.com/items?itemName=julialang.language-julia
[vim]: https://www.vim.org/download.php
[julia-vim]: https://github.com/JuliaEditorSupport/julia-vim
[windows-julia-installation]: https://julialang.org/downloads/platform/#windows
[mac-julia-installation]: https://julialang.org/downloads/platform/#macos
[linux-julia-installation]: https://julialang.org/downloads/platform/#linux_and_freebsd
[plots.jl]: https://docs.juliaplots.org/latest/
[malliselostus]: https://moodle.tuni.fi/pluginfile.php/39481/mod_resource/content/4/malliselostus.pdf
[lsqfit.jl]: https://github.com/JuliaNLSolvers/LsqFit.jl
[plots.jl attributes]: https://docs.juliaplots.org/latest/attributes/
[gr]: https://gr-framework.org/index.html
[jaot-compilation]: https://julialang.github.io/PackageCompiler.jl/dev/devdocs/sysimages_part_1/#Julia's-compilation-model-and-sysimages-1
