#=
# Laskentapohja työhön Kääntöheiluri [1]_.
# Samaan tapaan kuin demotöissä, laskut täydennetään funktion main sisälle.
# Tässä teille annetaan kuitenkin enemmän vastuuta omien muuttujanimien sun
# muiden keksimisen osalta. Muutamia vinkkejä, jotka ovat ymmärrettävissä
# demotöiden tekemisen jälkeen on kuitenkin on annettu.
#
# .. [1] https://tuni-my.sharepoint.com/personal/leena_partanen_tuni_fi/Documents/Fysiikan%20laboratorioty%C3%B6t/K%C3%A4%C3%A4nt%C3%B6heiluri_opintomoniste.pdf
=#

include(normpath(joinpath(
    @__DIR__, "..", "src", "tarkista_ympäristön_aktiivisuus.jl")))

#= Kirjastojen lataaminen =#

include(normpath(joinpath(
    @__DIR__, "..", "src", "kirjastojen_lataus.jl")))

#= Mittauspöytäkirjan lukuarvot ja listat (mielellään vakioina const) =#

const MITTAUSPÖYTÄKIRJA = missing

#= Apufunktioita (lisää tarvitsemasi funktiot tähän) =#

"""
Suoran mallifunktio sovitteiden muodostamista ja piirtämistäkin varten.
Tässä `x` on vaaka-akselin arvo ja `p` taas parametrien jono.
Suoran parametreja ovat kulmakerroin `p[1]` ja vakiotermi `p[2]`.
"""
function suoran_mallifunktio(x, p)
    missing # Katso jousivakion laskentapohjasta miten tämä tehdään,
            # jos ei muuten aukea.
end

"""
Piirtää aikaeron `T - T'` etäisyyden `d` funktiona.
"""
function piirrä_aikaero_etäisyyden_funktiona(
    etäisyydet::Vector,
    aikaerot::Vector,
    kulmakerroin::Number,
    vakiotermi::Number
)
    vaakaotsikko = "d (m)"
    pystyotsikko = "T - T' (s)"
    kuva = Plots.plot(legend=:bottomright, #= muut asetukset kuten otsikot=#)
    # Lisää kuvaajat kuvaan tässä.
    kuvan_polku = joinpath(KUVAKANSIO, "kääntöheiluri_aikaero_vs_etäisyys.pdf")
    Plots.savefig(kuva, kuvan_polku)
end

"""
Tätä nyt ainakin tarvitaan. Lisätkää tämän sisälle teorialuvussa
johtamanne kaava kiihtyvyydelle `g` ja parametreiksi laskennassa
tarvittavat muuttujat ja vakiot.
"""
function g(#=parametrit=#)
    missing
end

#= Päärutiini (täytä laskut tänne) =#

"""
Päärutiini. Lisätkää laskut tänne.
"""
function main()
    missing
end

# Pääfunktion kutsu.
main()
