#=
# Demotyön Jousivakio [1]_ laskentapohja.
# Täytä puuttuvat välivaiheet tiedoston lopusta löytyvään funktioon main.
#
# .. [1] Fysiikan laboratoriotöiden opintomoniste, Jousivakio,
#        päivitetty: 2021-05-19,
#        url: https://tuni-my.sharepoint.com/personal/leena_partanen_tuni_fi/Documents/Fysiikan%20laboratorioty%C3%B6t/Jousivakio_opintomoniste.pdf,
#        viitattu: 2021-05-28
=#

include(normpath(joinpath(
    @__DIR__, "..", "src", "tarkista_ympäristön_aktiivisuus.jl")))

#= Ladataan tarvitavat kirjastot =#

include(normpath(joinpath(
    @__DIR__, "..", "src", "kirjastojen_lataus.jl")))

#= Mittausdata vakioina (täytä nämä itse) =#

"""
Painovoiman aiheuttama kiihtyvyys Tampereella,
leveyspiirillä 61.49911° ja korkeudella ∼100 m.
"""
const g = 9.82003

"""
Venymäkokeessa käytetyt massat grammoista kilogrammmoihin muunnettuna
eli kerrottuna luvulla 10⁻³. Pisteittäinen kertomerkki `.*` kertoo jokaista
lista alkiota sen edessä olevalla luvulla.
"""
const VENYMÄKOKEEN_MASSAT_kg = 1e-3 * [
    i for i ∈ 1:10 # Korvaa nämä omilla lukuarvoilla oikeissa yksiköissä
]

"""
Venymäkokeessa mitatut jousen pituudet muunnettuna senttimetreistä metreihin
eli kerrottuna luvulla 10⁻².
"""
const VENYMÄKOKEEN_PITUUDET_m = 1e-2 * [
    0.0 # Täytä lukuarvot senttimetreinä tähän pilkuilla eroteltuina
]

"""
Venymäkokeessa mitatut venymät muunnettuna senttimetreistä metreihin
eli kerrottuna luvulla 10⁻².
"""
const VENYMÄKOKEEN_VENYMÄT_m = 1e-2 * [
    i for i ∈ 1:10 # ← Korvaa nämä omilla lukuarvoilla oikeissa yksiköissä
]

"""
Värähdyskokeen massat kilogrammoissa.
"""
const VÄRÄHDYSKOKEEN_MASSAT_kg = 1e-3 * [
    i for i ∈ 1:10 # Korvaa nämä omilla lukuarvoilla oikeissa yksiköissä
]

"""
Värähdyskokeessa 20 heilahduksessa kuluneet ajat sekunteina.
"""
const VÄRÄHDYSKOKEEN_20T_s = [
    0.0 # Täytä lukuarvot sekunteina tähän pilkuilla eroteltuina
]

"""
Selvitetään yhteen jaksonaikaan kuluneet ajat jakamalla
20 heilahdukseen kuluneet ajat alkioittain luvulla 20.
Piste jakomerkin edessä suorittaa saman laskutoimituksen
kaikille listan alkioille.
"""
const VÄRÄHDYSKOKEEN_T_s = VÄRÄHDYSKOKEEN_20T_s ./ 20

#= Apufunktioita =#

"""
Kirjasto LsqFit tarvitsee mallifunktion, jonka perusteella se tekee sovitteen.
Tässä tapauksessa sovite on teorialuvun mukaisesti suora, joten määritetään
sovitteen mukailevan suoraa. Huomaa, että kaikista laskutoimituksista käytetään
alkioittaisia versioita `.*` ja `.+` tavallisten `*` ja `+` sijaan.

Tässä `x` vastaa vaaka-akselin lukuarvoa, ja vektori `p` selvitettäviä
parametreja eli kulmakerrointa `p[1]` ja vakiotermiä `p[2]`.
"""
function suoran_mallifunktio(x, p)
    p[1] .* x .+ p[2]
end

"""
Piirtää mittapisteet ja näihin suoran sovitteen.
Ottaa parametreinaan tarvittavat mittapisteet ja sovitteesta otetut
vakiokertoimet, sekä tarvittavat otsikot ja kuvan nimen.
"""
function piirrä_pisteet_ja_suora(
    vaakadata,
    pystydata,
    kulmakerroin,
    vakiotermi,
    mittapisteiden_otsikko_selitelaatikossa,
    sovitteen_otsikko_selitelaatikossa,
    vaaka_akselin_otsikko,
    pystyakselin_otsikko,
    kuvan_nimi
)
    # Tallennetaan kuva samaan hakemistoon tämän tiedoston kanssa PDF-muodossa:
    kuvan_polku = joinpath(KUVAKANSIO, kuvan_nimi)
    println("\nPiirretään kuvaa polkuun \"$kuvan_polku\"…")

    # Luodaan kuva, johon kuvaajat voidaan piirtää:
    kuva = Plots.plot(
        xlabel=vaaka_akselin_otsikko,
        ylabel=pystyakselin_otsikko,
        legend=:bottomright) # Selitelaatikon sijainti

    # Mittapisteet piirretään luotuun kuvaan pisteinä (scatter).
    # Huomaa huutomerkki funktion nimen perässä.
    Plots.scatter!(
        kuva,
        vaakadata,
        pystydata,
        label=mittapisteiden_otsikko_selitelaatikossa,
        color=:steelblue)

    # Sovite piirretään kulmakertoimen ja vakiokertoimen avulla.
    # Lasketaan ensin sille omat vaakapisteet nollasta luvun 0.1 välein,
    # hieman yli mittadatan maksimiarvon:
    sovitteen_vaakadata = 0 : 0.1 : (vaakadata[end] + vaakadata[end] / 10)
    sovitteen_pystydata = kulmakerroin .* sovitteen_vaakadata .+ vakiotermi

    # Sitten piirretään sovite:
    Plots.plot!(
        kuva,
        sovitteen_vaakadata,
        sovitteen_pystydata,
        linestyle=:dash,
        label=sovitteen_otsikko_selitelaatikossa,
        color=:steelblue)

    println("Tallennetaan kuvaa. Tässä menee hetki…")
    Plots.pdf(kuva, kuvan_polku)
end

#= Itse laskut päärutiinissa =#

"""
Päärutiini, jota kutsutaan tiedoston pohjalla.
Määritä tänne laskujesi eteneminen samaan tapaan kuin
malliselostuksen laskentapohjassa.
"""
function main()

    println("\nAloitetaan laskut…")

    println("\n---------- Venymäkoe ----------\n")

    # Lasketaan aluksi venymään liittyvät voimat Newtonin toisen lain avulla.
    # Painot G = massalista * g, eli korvaa massalista + melu oikealla laskulla:
    melutaso = minimum(abs.(VENYMÄKOKEEN_MASSAT_kg))
    kohina = rand(-melutaso:melutaso/5:melutaso, length(VENYMÄKOKEEN_MASSAT_kg))
    G = VENYMÄKOKEEN_MASSAT_kg + kohina

    # Tulostetaan voimat selostusta varten luettavassa muodossa funktion `display` avulla.
    print("G = "); display(G);

    println("\n\nMuodostetaan venymäkokeen sovitetta…")

    alkuarviot_parametreille = [25.0, 0.5]

    # Itse sovitteen laskeminen
    venymäsovite = LsqFit.curve_fit(
        suoran_mallifunktio,      # sovitteen muoto
        VENYMÄKOKEEN_VENYMÄT_m,   # vaaka-akselin arvot
        G,                        # pystyakselin arvot
        alkuarviot_parametreille  # alkuarviot kulmakertoimelle ja vakiotermille
    )

    # Kopioidaan tehdystä sovitteesta halutut parametrit omiin muuttujiinsa.
    # Sovite pitää sisällään parametrilistan param:
    kulmakerroin_venymä = venymäsovite.param[1]
    vakiotermi_venymä   = venymäsovite.param[2]

    # Lasketaan myös standardivirheet kulmakerroimelle ja vakiotermille
    # sovitteen tehneen funktion paluuarvosta:
    venymävirheet = LsqFit.stderror(venymäsovite)

    # … ja otetaan ne ulos paluuarvosta:
    Δkulmakerroin_venymä = venymävirheet[1]
    Δvakiotermi_venymä   = venymävirheet[2]

    # Piirretään kuva kutsumalla yllä määriteltyä piirtofunktiota sopivilla
    # argumenteilla
    piirrä_pisteet_ja_suora(
        VENYMÄKOKEEN_VENYMÄT_m, # Vaaka-akselin arvot
        G,                      # Pystyakselin arvot
        kulmakerroin_venymä,
        vakiotermi_venymä,
        "mittapisteet",         # Mittapisteiden otsikko selitelaatikossa
        "Suora(mittapisteet)",  # Sovitteen otsikko selitelaatikossa
        "Δx (m)",                   # Vaaka-akselin otsikko
        "G (N)",                    # Pystyakselin otsikko
        "jousivakio_venymäkuva.pdf" # Kuvan nimi
    )

    println("\n---------- Jaksonaikakoe ----------\n")

    # Hyvin samaan tapaan kuin venymäkokeen tapauksessa. Täyttäkää tähän
    # puuttuvat vaiheet: vaaka- ja pystyakselien arvojen muodostus, sovitteen
    # teko niiden perusteella ja kuvan muodostus funktion
    # piirrä_pisteet_ja_suora avulla.

    kulmakerroin_jaksonaika = 0.0
    Δkulmakerroin_jaksonaika = 0.0

    #= Laskujen tulosten tulostus =#
    println("\n--------- Jousivakiot ---------\n")
    println("k_Δx = ($kulmakerroin_venymä ± $Δkulmakerroin_venymä) Nm⁻¹")
    println("k_T  = ($kulmakerroin_jaksonaika ± $Δkulmakerroin_jaksonaika) Nm⁻¹")
end

# Tämä funktion main kutsu käynnistää ohjelman, jos tätä skriptiä kutsutaan
# Julian komentorivillä komennolla include("jousivakio.jl")
main()
