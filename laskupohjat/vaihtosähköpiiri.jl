#=
# Laskentapohja työhön Vaihtosähköpiiri [1]_.
# Samaan tapaan kuin demotöissä, laskut täydennetään funktion main sisälle.
# Tässä teille annetaan kuitenkin enemmän vastuuta omien muuttujanimien sun
# muiden keksimisen osalta. Muutamia vinkkejä, jotka ovat ymmärrettävissä
# demotöiden tekemisen jälkeen on kuitenkin on annettu.
#
# .. [1] https://tuni-my.sharepoint.com/personal/leena_partanen_tuni_fi/Documents/Fysiikan%20laboratorioty%C3%B6t/Vaihtos%C3%A4hk%C3%B6piiri_opintomoniste.pdf
=#

include(normpath(joinpath(
    @__DIR__, "..", "src", "tarkista_ympäristön_aktiivisuus.jl")))

#= Kirjastojen lataaminen =#

include(normpath(joinpath(
    @__DIR__, "..", "src", "kirjastojen_lataus.jl")))

#= Mittauspöytäkirjan lukuarvot ja listat (mielellään vakioina const) =#

const MITTAUSPÖYTÄKIRJA = missing

#= Apufunktioita (lisää tarvitsemasi funktiot tähän) =#

"""
Laskee induktanssin kaavasta (23) muokatulla kaavalla.
Vinkki: tätä voi kutsua pisteittäin kokonaiselle listalle muodossa

    induktanssit_listassa = induktanssi.(lista) .
"""
function induktanssi(#= parametrit =#)
    missing # Ratkaisemasi lauseke
end

"""
Laskee vaihe-eron kuvan 7 kaavalla.
Vinkki: tätä voi kutsua pisteittäin kokonaiselle listalle muodossa

    vaihe_erot_listassa = kokeellinen_vaihe_ero.(lista) .
"""
function kokeellinen_vaihe_ero()
    missing # lauseke
end

"""
Laskee vaihe-eron kaavan (22) kohdannaisella.
Vinkki: tätä voi kutsua pisteittäin kokonaiselle listalle muodossa

    vaihe_erot_listassa = teoreettinen_vaihe_ero.(lista) .
"""
function teoreettinen_vaihe_ero()
    missing # lauseke
end

"""
Piirtää vaihe-erot logaritmiselle asteikolle taajuuden funktiona virhepalkkeineen.
Kutsu tätä funktiossa main sopivilla parametreilla.
"""
function piirrä_vaihe_ero_kuva(
        vaakataajuudet,
        koleelliset_vaihe_erot,
        teoreettiset_vaihe_erot,
        teoreettiset_virherajat, # Maksimivirheellä lasketut virherajat
        vaakaotsikko,
        pystyotsikko,
        kokeellinen_selite,     # Kokeellisten pisteiden selite selitelaatikossa
        teoreettinen_selite,    # Teoreettisten pisteiden selite selitelaatikossa
)
    # Asetetaan taustalla käytettäväksi piirtokirjastoksi GR
    Plots.gr()
    # Kuvan sijainti samaan kansioon tämän tiedoston kanssa.
    kuvan_polku = joinpath(KUVAKANSIO, "vaihtosähköpiiri_vaihe-erokuvaaja.pdf")
    # Luodaan kuva, jonka vaaka-akseli on logaritminen
    Plots.plot(xscale=:log10, #=lisää muut asetukset kuten akseleiden otsikot tähän=#)
    # Piirretään kuvaan teoreettiset mittapisteet y- eli
    # pystysuuntaisine virhepalkkeineen:
    Plots.plot(vaakataajuudet, teoreettiset_vaihe_erot,
        label=teoreettinen_selite, yerr=teoreettiset_virherajat)
    # Tallennetaan lopuksi kuva
    Plots.savefig(kuva, kuvan_polku)
end

#= Päärutiini (täytä laskut tänne) =#

"""
Päärutiini. Lisätkää laskut tänne.
"""
function main()
    missing
end

# Pääfunktion kutsu.
main()
