#=
# Laskentapohja työhön Langan ominaisvärähtelyt [1]_.
# Samaan tapaan kuin demotöissä, laskut täydennetään funktion main sisälle.
# Tässä teille annetaan kuitenkin enemmän vastuuta omien muuttujanimien sun
# muiden keksimisen osalta. Muutamia vinkkejä, jotka ovat ymmärrettävissä
# demotöiden tekemisen jälkeen on kuitenkin on annettu.
#
# .. [1] https://tuni-my.sharepoint.com/personal/leena_partanen_tuni_fi/Documents/Fysiikan%20laboratorioty%C3%B6t/Langanominaisv%C3%A4r%C3%A4htelyt_opintomoniste.pdf
=#

include(normpath(joinpath(
    @__DIR__, "..", "src", "tarkista_ympäristön_aktiivisuus.jl")))

#= Kirjastojen lataaminen =#

include(normpath(joinpath(
    @__DIR__, "..", "src", "kirjastojen_lataus.jl")))

#= Mittauspöytäkirjan lukuarvot ja listat (mielellään vakioina const) =#

"""
Painovoiman aiheuttama kiihtyvyys Tampereella,
leveyspiirillä 61.49911° ja korkeudella ∼100 m.
"""
const g = 9.82003

const MITTAUSPÖYTÄKIRJA = missing

#= Apufunktioita (lisää tarvitsemasi funktiot tähän) =#

"""
Miksiköhän tämä on tässä? Liittyy varmaankin jotenkin monisteen teorialukuun
ja suureen v(T) sovitteisiin…
"""
function neliöjuuren_mallifunktio(T, p)
    missing # Täytä tähän puuttuva lauseke
end

#= Päärutiini (täytä laskut tänne) =#

"""
Päärutiini. Lisätkää laskut tänne.
"""
function main()
    missing
end

# Pääfunktion kutsu.
main()
