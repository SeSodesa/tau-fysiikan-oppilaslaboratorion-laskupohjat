#=
# Laskentapohja työhön Ominaislämpö [1]_.
# Samaan tapaan kuin demotöissä, laskut täydennetään funktion main sisälle.
# Tässä teille annetaan kuitenkin enemmän vastuuta omien muuttujanimien sun
# muiden keksimisen osalta. Muutamia vinkkejä, jotka ovat ymmärrettävissä
# demotöiden tekemisen jälkeen on kuitenkin on annettu.
#
# .. [1] https://tuni-my.sharepoint.com/personal/leena_partanen_tuni_fi/Documents/Fysiikan%20laboratorioty%C3%B6t/Ominaisl%C3%A4mp%C3%B6_opintomoniste.pdf
=#

include(normpath(joinpath(
    @__DIR__, "..", "src", "tarkista_ympäristön_aktiivisuus.jl")))

#= Kirjastojen lataaminen =#

include(normpath(joinpath(
    @__DIR__, "..", "src", "kirjastojen_lataus.jl")))

#= Mittauspöytäkirjan lukuarvot ja listat (mielellään vakioina const) =#

const MITTAUSPÖYTÄKIRJA = missing

#= Apufunktioita (lisää tarvitsemasi funktiot tähän) =#

"""
Omnaislämmön laskeva funktio.
"""
function ominaislämpö(#= parametrit =#)
    missing # Täydentäkää lauseke tähän.
end

"""
Piirtää kuvan mittauksen etenemisestä annettujen argumenttien avulla.
Periaatteessa tarvittavia argumenttejahan riittää, koska esim. eri jaksot
halutaan piirtää omina mittasarjoinaan.
"""
function piirrä_lämpömittaus(
    ajat_esijakso::Vector,
    ajat_pääjakso::Vector,
    ajat_jälkijakso::Vector,
    lämpötilat_esijakso::Vector,
    lämpötilat_pääjakso::Vector,
    lämpötilat_jälkijakso::Vector,
    kulmakerroin_esijakso::Number,
    vakiotermi_esijakso::Number,
    kulmakerroin_jälkijakso::Number,
    vakiotermi_jälkijakso::Number,
    pystyviivan_ajankohta::Number,
    pystyviivan_alaraja::Number,
    pystyviivan_yläraja::Number,
    vaakaotsikko::String,
    pystyotsikko::String,
    kuvan_nimi::String
)
    kuvan_polku = joinpath(KUVAKANSIO, kuvan_nimi)
    println("Tallennetaan kuvaa polkuun $(kuvan_polku)…")
    kuva = Plots.plot(
        # Kuvan asetukset esim. funktion argumenteista
        xlabel="",
        ylabel="",
        legend=:bottomright)

    #= Toteutus tähän =#

    # Vinkkinä sovitteiden piirtämiseen käytettävät ajat koko kuvan leveydeltä
    sovitteiden_ajat = ajat_esijakso[begin]:0.01:ajat_jälkijakso[end]

    # Mittapisteet pisteinä
    Plots.scatter!(
        kuva,
        #= data tähän =#
        label="esijakso",
        color="blue")
    Plots.scatter!(
        kuva,
        #= data tähän =#
        label="pääjakso",
        color="orange")
    Plots.scatter!(
        kuva,
        #= data tähän =#
        label="jälkijakso",
        color="green")

    # Sovitteet viivoina
    Plots.plot!(
        kuva,
        #= data tähän =#
        label="Suora(esijakso)",
        color="blue")
    Plots.plot!(
        kuva,
        #= data tähän =#
        label="Suora(jälkijakso)",
        color="green")

    # Pystyviiva yhtenä viivana
    Plots.plot!(
        kuva,
        [pystyviivan_ajankohta for _ ∈ 1:2],
        [pystyviivan_alaraja, pystyviivan_yläraja],
        label="ΔT",
        color="black")

    # Lopuksi kuvan tallennus
    Plots.savefig(kuva, kuvan_polku)
end

#= Päärutiini (täytä laskut tänne) =#

"""
Päärutiini. Lisätkää laskut tänne.
"""
function main()
    missing
end

# Pääfunktion kutsu.
main()
