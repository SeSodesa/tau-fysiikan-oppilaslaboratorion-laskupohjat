#=
# Työn Vauhtipyörä [1]_ tulosten muodostusskripti.
# Täydennä laskujen välivaiheet tiedoston lopusta löytyvän funktion
# main sisälle. Lisäksi täydennä puuttuvien apufunktioiden sisältö
# tarkoituksenmukaiseksi.
#
# .. [1] Fysiikan laboratoriotöiden opintomoniste, Vauhtipyörä,
#        päivitetty: 2021-05-19,
#        url: https://tuni-my.sharepoint.com/personal/leena_partanen_tuni_fi/Documents/Fysiikan%20laboratorioty%C3%B6t/Vauhtipy%C3%B6r%C3%A4_opintomoniste.pdf,
#        viitattu: 2021-05-28
=#

include(normpath(joinpath(
    @__DIR__, "..", "src", "tarkista_ympäristön_aktiivisuus.jl")))

#= Ei ladata kirjastoja, koska ei tarvitse piirtää tai tehdä sovitteita =#
# import Plots      # kuvien piirtäminen
# import LsqFit     # sovitteet pienimmän nelilösumman menetelmällä
# using Printf      # Liukulukujen tulostusten siistiminen


#= Mittauspöytäkirjan sisältö vakioina (täydennä nämä) =#

"""
Vauhtipyörän massa kilogrammoissa.
"""
const M_kg  = 0.0

"""
Vauhipyörän massan mittaukseen käytettävän vaa'an tarkkuus kilogrammoissa.
"""
const ΔM_kg = 0.0

"""
Vauhtipyörää pyörittävän punnuksen massa kilograpmmoina.
"""
const m_kg = 0.0

"""
Vauhtipyörää pyörittävän punnuksen mittalaitteen epätarkkuus kilogrammoina.
"""
const Δm_kg = 0.0

"""
Narukehän säde metreihin muunnettuna.
"""
const r_m = 0.0

"""
Narukehän säteen mittalaitteen epätarkkuus metreihin muunnettuna.
"""
const Δr_m = 0.0

"""
Pyörimisajat sekunteina.
"""
const t_s = [0.0]
"""
Pyörimisajan mittalaitteen epätarkkuus sekunteina.
"""
const Δt_s = 0.0

"""
Vauhtipyörän säde metreissä.
"""
const R_m = 0.0

"""
Vauhtipyörän säteen mittalaitteen epätarkkuus metreissä.
"""
const ΔR_m = 0.0

"""
Kierrosmäärä ennen massan m irtoamista.
"""
const n1 = 0.0

"""
Kierrosmäärät massan `m` irtoamisen jälkeen listassa pilkuilla eroteltuina.
Murtoluvut kirjoitettaan muodossa 1//2
"""
const n2 = [0.0]

"""
Kierrosten epätarkkuus murtolukuna. Kaksinkertainen vinoviiva // luo murtoluvun.
"""
const Δn2  = 0.0

"""
Punnuksen alkukorkeus toistokokeessa metreinä.
"""
const h_m = 0.0

"""
Punnuksen alkukorkeuden mittalaitteen epätarkkuus toistokokeessa metreinä.
"""
const Δh_m = 0.0

"""
Jaksonaikojen kymmenkerrat sekunneissa pilkuilla , eroteltuina.
"""
const JAKSONAIKOJEN_KYMMENKERRAT_s = [0.0]

"""
Jaksonaikojen kymmenkertojen mittalaitteen epätarkkuus sekunneissa.
"""
const ΔJAKSONAIKOJEN_KYMMENKERRAT_s = 0.0

"""
Yksittäiset jaksonajat saadaan jakamalla jokainen jaksonajan
kymmenkerta kymmenellä. Pisteoperaattorilla ./ tämä hoituu kätevästi.
"""
const JAKSONAJAT_s = JAKSONAIKOJEN_KYMMENKERRAT_s ./ 10

"""
Myös jaksonajan epätarkkuus tulee jakaa kymmenellä, sillä

10⋅tulos = (lukuarvo ± epätarkkuus)
 ⇔ tulos = (lukuarvo ± epätarkkuus) / 10
         = (lukuarvo / 10) ± (epätarkkuus / 10) .

Se ei ole vektori, joten pisteittäistä operaattoria ei tarvita,
vaan voidaan tyytyä normaaliin jako-operaattoriin /.
"""
const ΔJAKSONAIKA = ΔJAKSONAIKOJEN_KYMMENKERRAT_s / 10

#= Apufunktioita =#

"""
Laskee teoreettisen hitausmomentin umpinaiselle sylinterille
annetun massan M ja säteen R perusteella. Funktio on toteutettu
esimerkkinä.
"""
function Ic_laskennallinen(M, R)
    1 / 2 * M * R^2
end

"""
Laskee hitausmomentin vauhtipyörän keskipisteen suhteen pyörähdyskokeen
tapauksessa. Täydennä tämän sisälle johtamasi lauseke ilman puolipistettä ;.
"""
function Ic_pyörimiskoe(m, r, g, t, h, n1, n2)
    missing
end

"""
Tämä tyhjä kuormitettu funktio on tässä vain, jotta työ kääntyisi ennen ylemmän
funktion toteutusta. Jos ohjelma tulostaa muistutuksen, et ole muistanut
lisätä funktion kutsupaikassa sen parametreiksi haluttuja suureita.
"""
function Ic_pyörimiskoe()
    println("Muistahan toteuttaa parametrillinen funktio Ic_pyörimiskoe…")
    missing
end

"""
Laskee hitausmomentin vauhtipyörän keskipisteen suhteen jaksonaikakokeen
tapauksessa. Täydennä tämän sisälle johtamasi lauseke ilman puolipistettä.
"""
function Ic_jaksonaikakoe(M, b, T, g)
    missing
end

"""
Tämä tyhjä kuormitettu funktio on tässä vain, jotta työ kääntyisi ennen ylemmän
funktion toteutusta. Jos ohjelma tulostaa muistutuksen, et ole muistanut
lisätä funktion kutsupaikassa sen parametreiksi haluttuja suureita.
"""
function Ic_jaksonaikakoe()
    println("Muistahan toteuttaa parametrillinen funktio Ic_jaksonaikakoe…")
    missing
end

"""
Laskee keskiarvon keskivirheen annetun keskiarvon ja mittausdatan perusteella.

Vinkki 1:
    kannattaa vilkaista malliselostuksen laskentapohjasta, miten tämä tulisi tehdä.
Vinkki 2:
    neliöjuurifunktiota voi kutsua sekä symbolilla `√`, että merkkijonolla `sqrt`.
Vinkki 3:
    muista pisteittäiset operaattorit: lauseke [1,2,3] .- 1 tuottaa [0,1,2].
"""
function keskiarvon_keskivirhe(keskiarvo::Number, mittausdata::Vector)
    missing # Kirjoita tähän lauseke, joka laskee keskiarvon keskivirheen.
            # Älä laita puolipistettä ; lausekkeen jälkeen, tai funktio ei palauta
            # lausekkeen arvoa.
end

"""
Yhdistää tilastollisen keskivirheen mittalaitteen epätarkkuuteen.
Katso Moodlen virhearvio-ohjeista [virhearviopruju]_, mikä tämän lauseke on.

.. [virhearviopruju] https://moodle.tuni.fi/pluginfile.php/39482/mod_resource/content/1/uusivirhearvio.pdf
"""
function keskivirheen_kasautumislaki(keskivirhe, mittalaitteen_epätarkkuus)
    missing # Täydennä tähän keskivirheen kasautumislain lauseke.
end

#=
# Jaksonaikakokeen maksimivirheen laskemiseen tarvittavat funktiot.
# Näiden summasta saadaan lopullinen maksimivirhe:
#
# maksimivirhe = osavirhe_1(parametrit) +
#     osavirhe_2(parametrit) + osavirhe_3(parametrit) + ⋯
=#

"""
Täydennä tähän johtamasi suureen `M` osuus maksimivirheestä |∂Ic/∂M|⋅ΔM.
Funktio `abs` laskee itseisarvon. Tässä ei tarvitse pisteittäisiä operaattoreita.
"""
function maksimivirheen_M_termi(M::Number, b::Number, T::Number, ΔM::Number)
    missing
end

"""
Täydennä tähän johtamasi suureen `b` osuuden maksimivirheestä |∂Ic/∂b|⋅Δb.
Funktio `abs` laskee itseisarvon.
"""
function maksimivirheen_b_termi(M::Number, b::Number, T::Number, Δb::Number)
    missing
end

"""
Täydennä tähän johtamasi suureen `T` osuuden maksimivirheestä |∂Ic/∂T|⋅ΔT.
Funktio `abs` laskee itseisarvon.

Huom:
    Muista, että ΔT on tässä keskivirheen kasautumislailla laskettu lukuarvo,
    kun kutsut tätä funktiota funktiossa main.
"""
function maksimivirheen_T_termi(M::Number, b::Number, T::Number, ΔT::Number)
    missing
end

#= Päärutiini =#

"""
Lisää laskujen välivaiheet tänne. Lisää apufunktioihin tarvittavat
sisällöt ja tänne funktioiden parametreiksi sulkujen () sisään
tarvittavat lukuarvot ja suureet.
"""
function main()
    println("\nAloitetaan laskut...\n")

    #= Pyörimiskoe =#
    Icp = Ic_pyörimiskoe()

    #= Jaksonaikakoe (sisältäen maksimivirheen) ΔIcT =#
    IcT = Ic_jaksonaikakoe()
    ΔIcT = missing

    #= Laskennollinen tulos =#
    Icl = Ic_laskennallinen(M_kg, R_m)

    #= Tulostetaan laskujen tulokset=#
    println("\n--------- Hitausmomentit ---------\n")
    println("Icp = $Icp kg⋅m²")
    println("IcT = ($IcT ± $ΔIcT) kg⋅m²")
    println("Icp = $Icl kg⋅m²")
    println("Valmista.")
end

# Funktiota main pitää muistaa kutsua, jotta ohjelma käynnistyisi.
main()
