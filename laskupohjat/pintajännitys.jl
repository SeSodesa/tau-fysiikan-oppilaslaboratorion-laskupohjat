#=
# Laskentapohja työhön Pintajännitys [1]_.
# Samaan tapaan kuin demotöissä, laskut täydennetään funktion main sisälle.
# Tässä teille annetaan kuitenkin enemmän vastuuta omien muuttujanimien sun
# muiden keksimisen osalta. Muutamia vinkkejä, jotka ovat ymmärrettävissä
# demotöiden tekemisen jälkeen on kuitenkin on annettu.
#
# .. [1] https://tuni-my.sharepoint.com/personal/leena_partanen_tuni_fi/Documents/Fysiikan%20laboratorioty%C3%B6t/Pintajannitys_opintomoniste.pdf
=#

include(normpath(joinpath(
    @__DIR__, "..", "src", "tarkista_ympäristön_aktiivisuus.jl")))

#= Kirjastojen lataaminen =#

include(normpath(joinpath(
    @__DIR__, "..", "src", "kirjastojen_lataus.jl")))

#= Mittauspöytäkirjan lukuarvot ja listat (mielellään vakioina const) =#

const MITTAUSPÖYTÄKIRJA = missing

#= Apufunktioita (lisää tarvitsemasi funktiot tähän) =#

"""
Laskee keskiarvon keskirvirheen annetun keskiarvon ja mittausdatan avulla.
"""
function keskiarvon_keskivirhe(keskiarvo::Number, mittausdata::Vector)
    missing # Lauseke tähän
end

"""
Laskee pintajännityksen opintomonisteen yhtälön (5) avulla.
"""
function pintajännitys_kaavalla_5(#= parametrit =#)
    missing # Pintajännityksen lauseke
end

"""
Laskee pintajännityksen opintomonisteen yhtälön (13) avulla.
"""
function pintajännitys_kaavalla_13(#= parametrit =#)
    missing # Pintajännityksen lauseke
end

"""
Laskee maksimivirheen pintajännitykselle annettujen osavirheiden summana.
Tämä funktio on *variaattinen* eli sitä kutsutaan muodossa

    ΔγSA = pintajännityksen_maksimivirhe(osavirhe1, osavirhe2, …)

Julia yhdistää mielivaltaisen määrän erillisiä parametreja yhdeksi listaksi,
jonka summa on helppo laskea standardikirjaston funktiolla `sum`.
"""
function pintajännityksen_maksimivirhe(osavirheet...)
    sum(osavirheet)
end

"""
Laskee stalagmomentrimittausten korjauskertoimen Lee--Chan--Pokagu-mallin avulla.
"""
function lee_chan_pokagu(suhde::Number)
    missing # Täydennä lauseke tähän
end

"""
Laskee stalagmomentrimittausten korjauskertoimen HG-mallin avulla.
"""
function hg(suhde::Number)
    missing # Täydennä lauseke tähän
end

"""
Laskee korjauskertoimen valitsemalla sopivan mallin suhteen r÷³√(V) perusteella.
"""
function stalagmometrin_korjauskerroin(suhde::Number)
    if 0.0 ≤ suhde < 1.20
        lee_chan_pokagu(suhde)
    elseif 1.20 ≤ suhde < 1.60
        hg(suhde)
    else
        println("Tuntematon suhde r÷³√(V)…")
        # Palautetaan nothing, jos jokin meni pieleen.
        # Tätä voi testata kutsupaikassa funktiolla isnothing(paluuarvo)
        nothing
    end
end


#= Päärutiini (täytä laskut tänne) =#

"""
Päärutiini. Lisätkää laskut tänne.
"""
function main()
    missing
end

# Pääfunktion kutsu.
main()
