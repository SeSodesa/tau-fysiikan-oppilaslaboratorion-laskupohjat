#=
# Laskentapohja työhön Gyroskooppi [1]_.
# Samaan tapaan kuin demotöissä, laskut täydennetään funktion main sisälle.
# Tässä teille annetaan kuitenkin enemmän vastuuta omien muuttujanimien sun
# muiden keksimisen osalta.
#
# .. [1] https://tuni-my.sharepoint.com/personal/leena_partanen_tuni_fi/Documents/Fysiikan%20laboratorioty%C3%B6t/Gyroskooppi_opintomoniste.pdf
=#

include(normpath(joinpath(
    @__DIR__, "..", "src", "tarkista_ympäristön_aktiivisuus.jl")))

#= Kirjastojen lataaminen =#

include(normpath(joinpath(
    @__DIR__, "..", "src", "kirjastojen_lataus.jl")))

#= Mittauspöytäkirjan lukuarvot ja listat (mielellään vakioina) =#

"""
Vauhtipyörän massa kilogrammoissa.
"""
const M_VAUHTIPYÖRÄ_kg = 0.0

"""
Lisäpunnuksen massan mittalaitteen tarkkuus kilogrammoissa.
"""
const ΔM_VAUHTIPYÖRÄ_kg = 0.0

"""
Lisäpunnuksen massa kilogrammoissa.
"""
const M_LISÄPUNNUS_kg = 0.0

"""
Lisäpunnuksen massan mittalaitteen tarkkuus kilogrammoissa.
"""
const ΔM_LISÄPUNNUS_kg = 0.0

"""
Lisämassan etäisyys gyroskoopin prekessioakselista.
"""
const LISÄMASSAN_ETÄISYYS_m = 0.0

"""
Lisämassan etäisyyden tarkkuus gyroskoopin prekessioakselista.
"""
const ΔLISÄMASSAN_ETÄISYYS_m = 0.0

"""
Kulmanopeudet ω (rpm) sarakevektorissa eli pilkuilla eroteltuina.
"""
const KULMANOPEUDET_rpm = [0.0]

"""
Prekessioajat sekunneissa sarakevektorissa eli pilkuilla eroteltuina.
"""
const PREKESSIOAJAT_s = [0.0]

#= Apufunktioita (lisää tarvitsemasi funktiot tähän) =#

"""
Tätä funktiota voi käyttää sekuntikellon lukeman muuntamiseksi sekunneiksi.
"""
function sekuntikellon_lukema_sekunneiksi(
    minuutit::Number,
    sekunnit::Number,
    sekunnin_sadasosat::Number
)
    60 * minuutit + sekunnit + sekunnin_sadaosat * 10^-2
end

"""
Miksiköhän tämä on tässä?
Liittyy ihan takuulla jonkin pyydetyn sovitteen tekemiseen…
"""
function hyperbelin_mallifunktio(ω, p)
end

"""
Piirtää suoran annettujen mittapisteiden ja sovitteen parametrien perusteella.
"""
function piirrä_suora(
    vaakamittaukset::Vector,
    pystymittaukset::Vector,
    kulmakerroin::Number,
    vakiotermi::Number
)
    vaakaotsikko = "ω⁻¹ (s)"
    pystyotsikko = "Ω (s⁻¹)"
    kuva = Plots.plot(
        legend=:bottomright, #= muut asetukset =#)
    kuvan_polku = joinpath(KUVAKANSIO, "gyroskooppi_suora.pdf")
    # Lisää mittapisteiden ja sovitteen piirto tähän
    missing
    Plots.savefig(kuva, kuvan_polku)
end

"""
Piirtää hyperbelin annettujen mittapisteiden ja sovitteiden parametrien perusteella.
"""
function piirrä_hyperbeli(
    vaakamittaukset::Vector,
    pystymittaukset::Vector,
    verrannollisuuskerroin::Number
)
    vaakaotsikko = "ω (s⁻¹)"
    pystyotsikko = "Ω (s⁻¹)"
    kuva = Plots.plot(
        legend=:topright, #= muut asetukset =#)
    kuvan_polku = joinpath(KUVAKANSIO, "gyroskooppi_kääntäen.pdf")
    # Lisää mittapisteiden ja sovitteen piirto tähän
    missing
    Plots.savefig(kuva, kuvan_polku)
end


#= Päärutiini (täytä laskut tänne) =#

"""
Päärutiini.
"""
function main()
    missing
end

# Pääfunktion kutsu.
main()
