#=
# Laskentapohja työhön Kontaktikulma [1]_.
# Samaan tapaan kuin demotöissä, laskut täydennetään funktion main sisälle.
# Tässä teille annetaan kuitenkin enemmän vastuuta omien muuttujanimien sun
# muiden keksimisen osalta. Muutamia vinkkejä, jotka ovat ymmärrettävissä
# demotöiden tekemisen jälkeen on kuitenkin on annettu.
#
# .. [1] https://tuni-my.sharepoint.com/personal/leena_partanen_tuni_fi/Documents/Fysiikan%20laboratorioty%C3%B6t/Kontaktikulma_opintomoniste.pdf
=#

include(normpath(joinpath(
    @__DIR__, "..", "src", "tarkista_ympäristön_aktiivisuus.jl")))

#= Kirjastojen lataaminen =#

include(normpath(joinpath(
    @__DIR__, "..", "src", "kirjastojen_lataus.jl")))

#= Mittauspöytäkirjan lukuarvot ja listat (mielellään vakioina const) =#

const MITTAUSPÖYTÄKIRJA = missing

#= Apufunktioita (lisää tarvitsemasi funktiot tähän) =#

"""
Laskee pintajännityksen opintomonisteen yhtälön (8) avulla.
"""
function pintajännitys(#= parametrit =#)
    missing # Pintajännityksen lauseke
end

"""
Laskee maksimivirheen pintajännitykselle annettujen osavirheiden summana.
Tämä funktio on *variaattinen* eli sitä kutsutaan muodossa

    ΔγSA = pintajännityksen_maksimivirhe(osavirhe1, osavirhe2, …)

Julia yhdistää mielivaltaisen määrän erillisiä parametreja yhdeksi listaksi,
jonka summa on helppo laskea standardikirjaston funktiolla `sum`.
"""
function pintajännityksen_maksimivirhe(osavirheet...)
    sum(osavirheet)
end


#= Päärutiini (täytä laskut tänne) =#

"""
Päärutiini. Lisätkää laskut tänne.
"""
function main()
    missing
end

# Pääfunktion kutsu.
main()
