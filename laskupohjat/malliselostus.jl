#=
Tämä on esimerkkiskripti malliselostuksen [1]_ tuloksien
laskemiseksi kielellä Julia. Voit ajaa tiedoston asentamalla
Julian osoitteesta https://julialang.org/downloads/ ja
kirjoittamalla komentorivillä (esim. Windowsin PowerShell)

  julia polku/tiedostoon/malliselostus.jl

Skripti tulostaa laskujen tuloksen ruudulle ja muodostaa kuvat työhakemistoon.

.. [1] Leena Partanen,
       Esimerkkiselostus kappaleen tiheyden määrittämisestä,
       url: https://moodle.tuni.fi/pluginfile.php/39481/mod_resource/content/4/malliselostus.pdf,
       muokattu: 2019-08-22,
       viitattu: 2021-05-22
=#

include(normpath(joinpath(
    @__DIR__, "..", "src", "tarkista_ympäristön_aktiivisuus.jl")))

#= Tarvittavien kirjastojen lataus =#

include(normpath(joinpath(
    @__DIR__, "..", "src", "kirjastojen_lataus.jl")))

#= Mittauspöytäkirjan tulokset vakioina =#
#
# Huom: Vaikka nämä ovat globaaleja vakioita, tulee ne silti välittää
# niitä käyttävien funktioiden parametreina käännösvirheiden välttämiseksi.

"""
Apuvakio muunnoskertoimelle.
"""
const MILLIT_PERUSYKSIKÖIKSI = 1e-3

"""
Särmiön leveydet listassa, muunnettuna metreiksi mittauspöytäkirjan millimetreistä.
"""
const LEVEYDET_m = MILLIT_PERUSYKSIKÖIKSI .* [
    44.50, 44.42, 44.31, 44.34, 44.24, 44.32, 44.28, 44.24, 44.12, 44.40,
]

"""
Leveyksien mittaustarkkuus millimetreistä metreihin muunnettuna.
"""
const ΔLEVEYS_m = 0.02 * MILLIT_PERUSYKSIKÖIKSI

"""
Särmiön pituudet listassa, muunnettuna metreiksi mittauspöytäkirjan millimetreistä.
"""
const PITUUDET_m = MILLIT_PERUSYKSIKÖIKSI .* [
    42.12, 42.24, 41.76, 41.50, 41.38, 41.26, 41.20, 40.96, 42.10, 41.36,
]

"""
Pituuksien mittaustarkkuus millimetreistä metreihin muunnettuna.
"""
const ΔPITUUS_m = ΔLEVEYS_m

"""
Särmiön paksuudet listassa, muunnettuna metreiksi mittauspöytäkirjan millimetreistä.
"""
const PAKSUUDET_m = MILLIT_PERUSYKSIKÖIKSI .* [
    1.40, 1.40, 1.30, 1.30, 1.34, 1.32, 1.12, 1.34, 1.36, 1.34
]

"""
Paksuuksien mittaustarkkuus millimetreistä metreihin muunnettuna.
"""
const ΔPAKSUUS_m = ΔLEVEYS_m

"""
Kappaleen massa kilogrammoissa.
"""
MASSA_kg = 51.063 * MILLIT_PERUSYKSIKÖIKSI
"""
Kappaleen massan epätarkkuus kilogrammoissa.
"""
ΔMASSA_kg = 0.1 * MILLIT_PERUSYKSIKÖIKSI

#= Tuloksien laskennassa käytettyjä funktioita =#

"""
Funktio ottaa parametrinaan vektorin (eli listan) lukuarvoja ja
palauttaa sen alkioiden aritmeettisen keskiarvon.
"""
function listan_keskiarvo(lista::Vector)
   sum(lista) / length(lista)
end

"""
Laskee suorakulmaisen särmiön tiheyden kaavalla (3) annettujen
massan, leveyden, pituuden ja paksuuden avulla.
"""
function särmiön_tiheys(massa, leveys, pituus, paksuus)
    massa / (leveys * pituus * paksuus)
end

"""
Laskee keskiarvon keskivirheen annetulle keskiarvolle ja mittausdatalle.

Huomaa, että operaattori `.-` suorittaa pisteittäisen erotuksen,
eli vähentää keskiarvon jokaisesta listan `mittausdata` alkiosta
erikseen ja palauttaa yhtä suuren listan, joka sisältää erotuksien tulokset.

Sama juttu pisteittäisen potenssioperaattorin `.^` kanssa.
Se suorittaa potenssiin korotuksen jokaiselle pisteittäisen
erotuksen tuottaman listan alkiolle erikseen.
"""
function keskiarvon_keskivirhe(keskiarvo::Number, mittausdata::Vector)
    n = length(mittausdata)
    √(sum((mittausdata .- keskiarvo) .^ 2 ) / (n * (n - 1)))
end

"""
Yhdistää tietyn suureen tilastollisen keskivirheen mittalaitteen
epätarkkuuteen keskivirheen kasautumislailla.
"""
function keskivirheen_kasautuminen(keskivirhe::Number, mittalaitteen_epätarkkuus::Number)
    √(keskivirhe^2 + mittalaitteen_epätarkkuus^2)
end

"""
Laskee massan mittauksen aiheuttaman osuuden maksimivirheestä.
Ottaa parametrinaan leveyden `a`, pituuden `b`, paksuuden `c`
sekä massan mittausepätarkkuuden `Δm`.
"""
function maksimivirheen_massatermi(a, b, c, Δm)
    abs(1 / (a * b * c)) * Δm
end

"""
Laskee leveyden mittauksen aiheuttaman osuuden maksimivirheestä.
Ottaa parametrinaan massan `m`, leveyden `a`, pituuden `b`, paksuuden `c`
sekä leveyden mittausepätarkkuuden `Δa`.
"""
function maksimivirheen_leveystermi(m, a, b, c, Δa)
    abs(-m / (a^2 * b * c)) * Δa
end

"""
Laskee pituuden mittauksen aiheuttaman osuuden maksimivirheestä.
Ottaa parametrinaan massan `m`, leveyden `a`, pituuden `b`, paksuuden `c`
sekä pituuden mittausepätarkkuuden `Δb`.
"""
function maksimivirheen_pituustermi(m, a, b, c, Δb)
    abs(-m / (a * b^2 * c)) * Δb
end

"""
Laskee paksuuden mittauksen aiheuttaman osuuden maksimivirheestä.
Ottaa parametrinaan massan `m`, leveyden `a`, pituuden `b`, paksuuden `c`
sekä paksuuden mittausepätarkkuuden `Δc`.
"""
function maksimivirheen_paksuustermi(m, a, b, c, Δc)
    abs(-m / (a * b * c^2)) * Δc
end

"""
Piirtää annetun toistokokeen arvot käyttäen kirjastoa Plots,
joka on ladattu tiedoston alussa.
"""
function piirrä_mittausdata(
    leveydet::Vector,
    leveyksien_virheiden_indeksit::Vector,
    pituudet::Vector,
    pituuksien_virheiden_indeksit::Vector,
    paksuudet::Vector,
    paksuuksien_virheiden_indeksit::Vector,
    vaakaotsikko::String,
    pystyotsikko::String,
    kuvan_nimi::String
)
    kuvan_sijainti = joinpath(KUVAKANSIO, kuvan_nimi)
    println("Piirretään kuvaa mittaustuloksista sijaintiin $kuvan_sijainti…")
    println("Luodaan kuva asetuksineen…")
    kuva = Plots.plot(
        xlabel=vaakaotsikko, # Vaaka-akselin otsikko
        ylabel=pystyotsikko, # Pystyakslein otsikko
        legend=:right        # Selitelaatikon sijanti.
                             # Huomaa kaksoispiste asetukselle annetun arvon edessä.
    )
    println("Piirretään leveydet kuvaan…")
    # Huomaa huutomerkki funktion nimen lopussa. Se tarkoittaa, että funktio
    # muokkaa sille annettua olemassaolevaa kuvaa, eikä luo siitä kopiota:
    leveys_vaaka_akseli = 1:length(leveydet)
    Plots.scatter!(
        kuva,                   # Ensimmäisenä yllä muodostettu kuva
        leveys_vaaka_akseli,    # Vaaka-akselin arvot taulukossa
        leveydet,               # Pystyakselin arvot taulukossa
        label="leveydet",
        marker=:circle,
        color=:blue
    )
    if !isempty(leveyksien_virheiden_indeksit)
        println("Piirretään leveyksien karkea mittausvirhe kuvaan…")
        Plots.scatter!(
            kuva,
            pituuksien_virheiden_indeksit,
            pituudet[pituuksien_virheiden_indeksit],
            label="levelyksien karkea virhe",
            marker=:x,
            color=:blue
        )
    end
    println("Piirretään pituudet kuvaan…")
    pituus_vaaka_akseli = 1:length(pituudet)
    Plots.scatter!(
        kuva,
        pituus_vaaka_akseli,
        pituudet,
        label="pituudet",
        marker=:utriangle,
        color=:orange
    )
    if !isempty(pituuksien_virheiden_indeksit)
        println("Piirretään paksuuksien karkea mittausvirhe kuvaan…")
        Plots.scatter!(
            kuva,
            pituuksien_virheiden_indeksit,
            pituudet[pituuksien_virheiden_indeksit],
            label="pituuksien karkea virhe",
            marker=:x,
            color=:orange
        )
    end
    println("Piirretään paksuudet kuvaan…")
    paksuus_vaaka_akseli = 1:length(paksuudet)
    Plots.scatter!(
        kuva,
        paksuus_vaaka_akseli,
        paksuudet,
        label="paksuudet",
        marker=:plus,
        color=:green
    )
    if !isempty(paksuuksien_virheiden_indeksit)
        println("Piirretään paksuuksien karkea mittausvirhe kuvaan…")
        Plots.scatter!(
            kuva,
            paksuuksien_virheiden_indeksit,
            paksuudet[paksuuksien_virheiden_indeksit],
            label="paksuuksien karkea virhe",
            marker=:x,
            color=:green
        )
    end
    print("Tallennetaan kuvaa…")
    Plots.savefig(kuva, kuvan_sijainti)
    println(" Ok")
end

"""
Piirtää yhden mittapistelistan arvot ja näiden keskiarvon yhtenä viivana.
"""
function piirrä_mittapisteet_ja_keskiarvo(
    mittapisteet::Vector,
    virheiden_indeksit::Vector,
    vaakaotsikko::String,
    pystyotsikko::String,
    nimi_selitelaatikossa::String,
    kuvatiedoston_nimi::String
)
    kuvan_polku = joinpath(KUVAKANSIO, kuvatiedoston_nimi)
    println("Piirretään mittapisteitä ja niiden keskiarvoa sijaintiin $kuvan_polku…")
    println("Luodaan kuva asetuksineen…")
    kuva = Plots.plot(
        xlabel=vaakaotsikko, # Vaaka-akselin otsikko
        ylabel=pystyotsikko, # Pystyakslein otsikko
        legend=:bottomleft   # Selitelaatikon sijainti
    )
    # Piirretään mittapisteet pistejoukkona
    println("Mittapisteet…")
    Plots.scatter!(
        kuva,
        mittapisteet,
        label=nimi_selitelaatikossa,
        color=:green,
        marker=:+
    )
    if !isempty(virheiden_indeksit)
        println("Mittausvirheet…")
        # Karkea mittausvirhe
        Plots.scatter!(
            kuva,
            virheiden_indeksit,
            mittapisteet[virheiden_indeksit],
            label="karkea virhe",
            marker=:x,
            color=:red
        )
    end
    # Lasketaan ja piirretään keskiarvo yhtenä viivana
    println("Keskiarvo…")
    keskiarvo = sum(mittapisteet) / length(mittapisteet)
    Plots.plot!(
        kuva,
        [1, length(mittapisteet)],
        [keskiarvo, keskiarvo],
        color=:green,
        label="Keskiarvo($nimi_selitelaatikossa)"
    )
    println("Tallennetaan kuvaa…")
    Plots.savefig(kuva, kuvan_polku)
end

"""
Päärutiini. Kutsuu yllä määriteltyjä funktioita ja tuottaa halutut tulokset.
"""
function main()
    # Tyhjää tilaa tulostuksien alkuun
    println()

    # Piirretään mittausdata PDF-muodossa
    piirrä_mittausdata(
        LEVEYDET_m, [],   # Leveydet ja niiden virheiden indeksit listassa
        PITUUDET_m, [],   # Pituudet ja niiden virheiden indeksit listassa
        PAKSUUDET_m, [7], # Paksuudet ja niiden virheiden indeksit listassa
        "mittauskerta",
        "Mitat (m)",
        "malliselostuksen_mittausdata.pdf"
    )

    # Piirretään paksuudet tarkemmin SVG-muodossa, sillä niissä esiintyi karkea
    # virhe
    piirrä_mittapisteet_ja_keskiarvo(
        PAKSUUDET_m, [7], # Mittapisteet ja niiden virheiden indeksit listassa
        "Mittauskerta",
        "Mitta (m)",
        "mittapisteet",
        "malliselostuksen_paksuudet_tarkemmin.pdf",
    )

    println("\n--------- Luku 5: laskut ja tulokset---------\n")

    # Lasketaan ensin keskiarvot yllä määritellyn funktion avulla:
    println("Lasketaan keskiarvoja a_ka, b_ka ja c_ka…")
    leveyksien_ka = listan_keskiarvo(LEVEYDET_m)
    pituuksien_ka = listan_keskiarvo(PITUUDET_m)

    # Poistetaan paksuuksista karkea mittausvirhe.
    # Merkintä lista[1:7] valitsee listasta alkiot 1--7.
    # Listoja voi laittaa pystysuunnassa peräkkäin syntaksilla [lista1; lista2].
    println(
        "Poistetaan paksuuksista 7. alkio (karkea mittausvirhe) " *
        "ennen keskiarvon laskua…"
    )
    paksuudet_ilman_karkeaa_virhettä = [
        PAKSUUDET_m[1:6];
        PAKSUUDET_m[8:end]
    ]
    paksuuksien_ka = listan_keskiarvo(paksuudet_ilman_karkeaa_virhettä)

    # Tulostetaan keskiarvot
    println("a_ka = $leveyksien_ka m")
    println("b_ka = $pituuksien_ka m")
    println("c_ka = $paksuuksien_ka m")

    # Lasketaan itse tiheys keskiarvoista:
    tiheys = särmiön_tiheys(MASSA_kg, leveyksien_ka, pituuksien_ka, paksuuksien_ka)

    println("\n--------- Luku 6: virhearvio ---------\n")

    println("Lasketaan keskiarvon keskivirheitä Δa_ka, Δb_ka ja Δc_ka…")
    Δa_ka = keskiarvon_keskivirhe(leveyksien_ka, LEVEYDET_m)
    Δb_ka = keskiarvon_keskivirhe(pituuksien_ka, PITUUDET_m)
    Δc_ka = keskiarvon_keskivirhe(paksuuksien_ka, paksuudet_ilman_karkeaa_virhettä)

    println("Yhdistetään keskiarvon keskivirheitä " *
        "mittalaitteiden epätarkkuuksiin keskivirheen kasautumislailla…")
    Δa = keskivirheen_kasautuminen(Δa_ka, ΔLEVEYS_m)
    Δb = keskivirheen_kasautuminen(Δb_ka, ΔPITUUS_m)
    Δc = keskivirheen_kasautuminen(Δc_ka, ΔPAKSUUS_m)

    # Lasketaan osavirheet maksimivirheestä:
    ΔM = maksimivirheen_massatermi(leveyksien_ka, pituuksien_ka, paksuuksien_ka, ΔMASSA_kg)
    ΔA = maksimivirheen_leveystermi(MASSA_kg, leveyksien_ka, pituuksien_ka, paksuuksien_ka, Δa)
    ΔB = maksimivirheen_pituustermi(MASSA_kg, leveyksien_ka, pituuksien_ka, paksuuksien_ka, Δb)
    ΔC = maksimivirheen_paksuustermi(MASSA_kg, leveyksien_ka, pituuksien_ka, paksuuksien_ka, Δc)
    Δtiheys = ΔM + ΔA + ΔB + ΔC

    # Tulostetaan osavirheet suurimman määrittämiseksi
    println("\nMaksimivirheen osavirheet:")
    println("   |∂ρ/∂m|Δm = $(ΔM) kgm⁻³")
    println("   |∂ρ/∂a|Δa = $(ΔA) kgm⁻³")
    println("   |∂ρ/∂b|Δb = $(ΔB) kgm⁻³")
    println("   |∂ρ/∂c|Δc = $(ΔC) kgm⁻³ ← Miksi tämä on näin iso?")
    println("∑|∂ρ/∂xᵢ|Δxᵢ = $(Δtiheys) kgm⁻³")

    println(
        "\nTarkastele suurimman osavirheen mittalaitteen tarkkuuden ja mitattujen" *
        " lukuarvojen suhdetta. Mitä huomaat? Mikä muutos pitäisi tehdä, jotta" *
        " suureen c mittauksen aiheuttama osavirhe ei olisi niin suuri?"
    )

    # Lopputuloksen ilmoitus
    println("\nρ = ($tiheys ± $Δtiheys) kgm⁻³.")
end

# Pääfunktion kutsu kaikkien määrittelyjen jälkeen.
# Tämä käynnistää ohjelman, kun sitä kutsutaan komentoriviltä.
main()
