# Laskupohjat

Tämä kansio sisältää TAUn fysiikan oppilaslaboratorion töiden tulosten
muodostamiseen tarvittavat laskupohjat. Tiedostossa `malliselostus.jl` on
esimerkki [malliselostuksen][malliselostus] tulosten muodostamisesta sekä kuvan
piirtämisestä. Demotyön [jousivakio][jousivakio] laskupohjaan `jousivakio.jl` on
myös sisällytetty valmiiksi ohjeet lineaarisen regression käyttöön
Julia-kirjaston [LsqFit.jl][lsqfit] avulla.

[malliselostus]:https://moodle.tuni.fi/pluginfile.php/39481/mod_resource/content/4/malliselostus.pdf
[jousivakio]: https://tuni-my.sharepoint.com/personal/leena_partanen_tuni_fi/Documents/Fysiikan%20laboratorioty%C3%B6t/Jousivakio_opintomoniste.pdf
[lsqfit]: https://github.com/JuliaNLSolvers/LsqFit.jl/tree/master
