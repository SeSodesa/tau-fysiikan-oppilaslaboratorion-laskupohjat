#=
# Laskentapohja työhön Coulombin laki [1]_.
# Samaan tapaan kuin demotöissä, laskut täydennetään funktion main sisälle.
# Tässä teille annetaan kuitenkin enemmän vastuuta omien muuttujanimien sun
# muiden keksimisen osalta. Muutamia vinkkejä, jotka ovat ymmärrettävissä
# demotöiden tekemisen jälkeen on kuitenkin on annettu.
#
# .. [1] https://tuni-my.sharepoint.com/personal/leena_partanen_tuni_fi/Documents/Fysiikan%20laboratorioty%C3%B6t/Coulombin_laki_opintomoniste.pdf
=#

include(normpath(joinpath(
    @__DIR__, "..", "src", "tarkista_ympäristön_aktiivisuus.jl")))

#= Kirjastojen lataaminen =#

include(normpath(joinpath(
    @__DIR__, "..", "src", "kirjastojen_lataus.jl")))

#= Mittauspöytäkirjan lukuarvot ja listat (mielellään vakioina const) =#

"""
Moodlesta löytyvä torsiovakio.
"""
const TORSIOVAKIO = 0.0;

"""
Jännite, jolla pallot on varattu etäisyyden vaikutuksen tutkimuksen aikana.
"""
const PALLOJEN_VARAUSJÄNNITE = 0.0;

"""
Pallojen väliset etäisyydet etäisyyden vaikutuksen tutkimisen aikana.
"""
const VARAUSETÄISYYDET = [0.0]

"""
Etäisyyden vaikutuksen tutkimiseen liittyvät kulmat. Korvaa nollat kulmien arvoilla.

Huom: Julian matriisit ovat Matlabin tapaan sarakemajorantteja,
eli samaan mittaussarjaan liittyvät arvot kannattaa sijoittaa sarakkeittain.
Välilyönnit erottavat sarakkeet, puolipiste ; rivit.
"""
const KULMAT = [
    0 0 0;
    0 0 0;
    0 0 0;
    0 0 0;
    0 0 0;
    0 0 0;
    0 0 0;
    0 0 0;
    0 0 0;
    0 0 0;
]

"""
Lista torsiovaa'an pallon jännitteistä varauksen vaikutuksen tutkimisen aikana.
"""
const TORSIOVAA_AN_PALLON_JÄNNITTEET = [0.0]

"""
Liikuteltavan pallon jännitteet varauksen vaikutuksen tutkimisen aikana.
"""
const LIIKUTELTAVAN_PALLON_JÄNNITTEET = [0.0]

"""
Pallojen väliset etäisyydet varauksen vaikutuksen tutkimisen aikana.
"""
const PALLOJEN_ETÄISYYDET = [0.0]

#= Apufunktioita (lisää tarvitsemasi funktiot tähän) =#

"""
Laskee korjauskertoimen B pallomaiselle eristekappaleelle.

Vinkki: Julia kääntää jokaisesta skalaarimuotoisesta funktiosta
myös pisteittäisen version. Tätäkin funktiota voi kutsua kokonaiselle
listalle tai taulukolle kerralla muodossa

    korjauskerroinlista = korjauskerroin.(lista_parametreja_1, lista_parametreja_2, …)

Parametrilistojen pitää vain olla saman kokoisia.
"""
function korjauskerroin(#= parametrit =#)
    missing # Lauseke
end

"""
Laskee korjauskertoimen B avulla korjatun kulman arvon.

Vinkki: Tätäkin funktiota voi kutsua kokonaiselle kulmavektorille
kerralla, funktion pistemäisessä muodossa korjattu_kulma..
"""
function korjattu_kulma(#= parametrit =#)
    missing # Lauseke
end

"""
Piirtää Coulombin vakion selvittämiseen liittyvä suoran
"""
function piirrä_suora_ja_pisteet(
    kuvan_nimi,
    vaakadata,
    pystydata,
    kulmakerroin,
    vakiotermi,
    vaakaotsikko,
    pystyotsikko,
    datan_nimi_selitelaatikossa,
    sovitteen_nimi_selitelaatikossa,
    # :right, :topright, :top, :topleft, :left, :bottomleft, jne.
    selitelaatikon_sijainti
)
    # Sijoitetaan kuva samaan hakemistoon tämän tiedoston kanssa:
    tiedoston_polku = joinpath(KUVAKANSIO, tiedoston_nimi)
    # Luodaan kuva ja lisätään siihen kuvaajat sekä tekstit
    kuva = Plots.plot(#= Eräät asetukset löytyvät jousivakion työpohjasta =#)
    #= Lisää kuvaajien piirtäminen tähän =#
    # Tallennetaan kuva
    Plots.savefig(kuva, kuvan_polku)
end

#= Päärutiini (täytä laskut tänne) =#

"""
Päärutiini. Lisätkää laskut tänne.
"""
function main()
    missing
end

# Pääfunktion kutsu.
main()
