#=
# Laskentapohja työhön Elektronin ominaisvaraus [1]_.
# Samaan tapaan kuin demotöissä, laskut täydennetään funktion main sisälle.
# Tässä teille annetaan kuitenkin enemmän vastuuta omien muuttujanimien sun
# muiden keksimisen osalta. Muutamia vinkkejä, jotka ovat ymmärrettävissä
# demotöiden tekemisen jälkeen on kuitenkin on annettu.
#
# .. [1] https://tuni-my.sharepoint.com/personal/leena_partanen_tuni_fi/Documents/Fysiikan%20laboratorioty%C3%B6t/Elektronin_ominaisvaraus_opintomoniste.pdf
=#

include(normpath(joinpath(
    @__DIR__, "..", "src", "tarkista_ympäristön_aktiivisuus.jl")))

#= Kirjastojen lataaminen =#

include(normpath(joinpath(
    @__DIR__, "..", "src", "kirjastojen_lataus.jl")))

#= Mittauspöytäkirjan lukuarvot ja listat (mielellään vakioina const) =#

"""
Helmholtzin kelan kierrosmäärä.
"""
const KELAN_KIERROKSET = 0

"""
Elektronisuihkun halkaisijat d 6×6 matriisissa.
Puolipisteillä ; erotellaan rivit ja välilyönneillä sarakkeet.
Korvaa nollat datallasi.
"""
const HALKAISIJAT = [
    0 0 0 0 0 0;
    0 0 0 0 0 0;
    0 0 0 0 0 0;
    0 0 0 0 0 0;
    0 0 0 0 0 0;
    0 0 0 0 0 0;
]

"""
Säteiden d tuottamiseen käytetyt virrat vektorissa.
"""
const VIRRAT = [0.0]

"""
Säteiden d tuottamiseen käytetyt jännitteet vektorissa.
"""
const JÄNNITTEET = [0.0]

#= Apufunktioita (lisää tarvitsemasi funktiot tähän) =#

"""
Luokittelee annetut arvot kuuteen eri luokkaan,
kasvattaen luokkaa vastaavan histogrammin ämpärin kokoa.
Tätä käytetään histogrammin muodostavan funktion sisällä.
"""
function histogrammin_6_suodatin(histogrammi, luokkien_rajat::Vector, arvo::Number)
    if luokkien_rajat[1]-eps() <= arvo < luokkien_rajat[2]
        histogrammi[1] += 1
    elseif luokkien_rajat[2]   <= arvo < luokkien_rajat[3]
        histogrammi[2] += 1
    elseif luokkien_rajat[3]   <= arvo < luokkien_rajat[4]
        histogrammi[3] += 1
    elseif luokkien_rajat[4]   <= arvo < luokkien_rajat[5]
        histogrammi[4] += 1
    elseif luokkien_rajat[5]   <= arvo < luokkien_rajat[6]
        histogrammi[5] += 1
    elseif luokkien_rajat[6]   <= arvo <= luokkien_rajat[7] + eps()
        histogrammi[6] += 1
    else
        # Näin pitkässä ehtorimpsussa pitäisi aina tehdä virhetarkastelu
        # else-haarassa.
        println("Alkio $arvo ei kuulu mihinkään luokkaan?")
    end
end

"""
Muodostaa histogrammin, joka tässä ajatellaan listana kokonaislukuja.
Palauttaa lisäksi luokkien rajat, joita voi käyttää vaikkapa kuvaajan
vaaka-akselin piirtämisessä…

Käyttäjän tulee antaa ohjeena kuinka moneen tasapaksuun luokkaan histogrammi
jaetaan ja suodatinfunktio, joka kasvattaa histogrammin pylväitä tarpeen mukaan.
Annetun luokkien lukumäärän ja suodatinfunktion ehtohaarojen lukumäärien
tulee osua yhteen. Tässä työssä kannattaa käyttää suodattimena funktiota
`histogrammin_6_suodatin`.
"""
function histogrammi(
    datamatriisi,
    luokkien_lukumäärä::Int,
    suodatinfunktio::Function
)
    # Luodaan luokkien lukumäärän pituinen histogrammi listarakentajalla:
    # 1. https://en.wikipedia.org/wiki/Set-builder_notation
    # 2. https://docs.julialang.org/en/v1/manual/arrays/#man-comprehensions
    histogrammi = [ 0 for i in 1:luokkien_lukumäärä ]
    # Selvitetään datamatriisista suurin ja pienin alkio,
    # sekä lukuvälin pituus
    pienin = minimum(datamatriisi)
    suurin = maximum(datamatriisi)
    lukuvälin_pituus = suurin - pienin
    # Muodostetaan luokkien rajat jakamalla tämä lukuväli tasan
    # listan rakentasyntaksilla
    luokkien_rajat = [ i/luokkien_lukumäärä * lukuvälin_pituus + pienin
                       for i in 0:luokkien_lukumäärä ]
    # Kerrytetään histogrammin pylväitä silmukassa antamalla suodattimelle
    # kerrytettävä histogrammi, luokkien rajat ja tarkasteltava alkio.
    for alkio in datamatriisi
        suodatinfunktio(histogrammi, luokkien_rajat, alkio)
    end
    # Funktion viimeinen puolipisteetön lauseke palautetaan funktiosta.
    # Tämä funktio palauttaa kaksi arvoa, jotka voi ottaa vastaan funktion
    # kutsupaikassa pilkuilla eroteltuina:
    # histogrammi, luokkien_rajat = histogrammi_6(datamatriisi).
    histogrammi, luokkien_rajat
end

"""
Piirtää annetun histogrammin (listan kokonaislukuja).
Voit määrittää tälle funktiolle lisäparametreja, jos
haluat välittää sille esimerkiksi vaaka-akselin otsikon
ja niin edespäin.
"""
function piirrä_histogrammi(histogrammi, luokkien_rajat, kuvan_nimi)
    # Kuvan sijainti samaan hakemistoon tämän tiedoston kanssa:
    kuvan_polku = joinpath(KUVAKANSIO, kuvan_nimi)
    # Luo kuva samalla tavalla kuin aiemmin komennolla Plots.plot(asetukset)
    # ja syötä siihen pylväsdiagrammi sellaisenaan komennolla
    # Plots.bar!(kuva, data, muut_asetukset). Huomaa huutomerkki funktion nimen perässä.
    # Jos vaaka-akselin dataa ei ole annettu, Plots.bar! piirtää vaaka-akselille
    # kokonaislukuja eli tässä luokkien indeksit.
    #
    # Tallenna kuva komennolla Plots.savefig(kuva, kuvan_polku)
end

"""
Laskee ominaisvarauksen teorian perusteella.

Vinkki: jos haluatte laskea ominaisvarauksen kokonaiselle matriisille
kerralla ja palauttaa vastaavankokoisen tulosmatriisin,
kannattaa tästä funktiosta kutsua pisteittäistä versiota,
jonka Julia luo automaattisesti tämän skalaariversion kohdatessaan:

    ominaisvarausmatriisi = ominaisvaraus.(matriisi)
"""
function ominaisvaraus(#= parametrit =#)
    missing # Lauseke tänne
end

#= Päärutiini (täytä laskut tänne) =#

"""
Päärutiini. Lisätkää laskut tänne.
"""
function main()
    missing
end

# Pääfunktion kutsu.
main()
