#=
# Laskentapohja työhön Dielektrisyys [1]_.
# Samaan tapaan kuin demotöissä, laskut täydennetään funktion main sisälle.
# Tässä teille annetaan kuitenkin enemmän vastuuta omien muuttujanimien sun
# muiden keksimisen osalta. Muutamia vinkkejä, jotka ovat ymmärrettävissä
# demotöiden tekemisen jälkeen on kuitenkin on annettu.
#
# .. [1] https://tuni-my.sharepoint.com/personal/leena_partanen_tuni_fi/Documents/Fysiikan%20laboratorioty%C3%B6t/Dielektrisyys_opintomoniste.pdf
=#

include(normpath(joinpath(
    @__DIR__, "..", "src", "tarkista_ympäristön_aktiivisuus.jl")))

#= Kirjastojen lataaminen =#

include(normpath(joinpath(
    @__DIR__, "..", "src", "kirjastojen_lataus.jl")))

#= Mittauspöytäkirjan lukuarvot ja listat (mielellään vakioina const) =#

const MITTAUSPÖYTÄKIRJA = missing

#= Apufunktioita (lisää tarvitsemasi funktiot tähän) =#

"""
Laskee tyhjiön permittiivisyyden kaavalla (26).

Vinkki: jos laskun lähtöarvot sijaitsevat vektoreissa,
voi tästä funktiosta kutsua pisteittäistä versiota

    permittiivisyydet = tyhjiön_permittiivisyys.(vektori_1, vektori_2, …) ,

kunhan lähtöarvovektorit ovat samankokoisia. Julia osaa laajentaa
skalaarit vektoreiksi pisteittäisiä funktioita kutsuttaessa.
"""
function tyhjiön_permittiivisyys(#= parametrit =#)
    missing # lauseke
end

"""
Tätä tarvitaan sovitteen tekemiseen kirjaston LsqFit avulla.
Katso työn Jousivakio laskupohjasta, miten tämä tulisi kirjoittaa.
"""
function suoran_mallifunktio(#= parametrit =#)
    missing # lauseke
end

#= Päärutiini (täytä laskut tänne) =#

"""
Päärutiini. Lisätkää laskut tänne.
"""
function main()
    missing
end

# Pääfunktion kutsu.
main()
