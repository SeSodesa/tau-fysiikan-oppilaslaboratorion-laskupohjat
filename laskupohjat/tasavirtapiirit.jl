#=
# Laskentapohja työhön Tasavirtapiirit [1]_.
# Samaan tapaan kuin demotöissä, laskut täydennetään funktion main sisälle.
# Tässä teille annetaan kuitenkin enemmän vastuuta omien muuttujanimien sun
# muiden keksimisen osalta. Muutamia vinkkejä, jotka ovat ymmärrettävissä
# demotöiden tekemisen jälkeen on kuitenkin on annettu.
#
# .. [1] https://tuni-my.sharepoint.com/personal/leena_partanen_tuni_fi/Documents/Fysiikan%20laboratorioty%C3%B6t/Tasavirtapiirit_opintomoniste.pdf
=#

include(normpath(joinpath(
    @__DIR__, "..", "src", "tarkista_ympäristön_aktiivisuus.jl")))

#= Kirjastojen lataaminen =#

include(normpath(joinpath(
    @__DIR__, "..", "src", "kirjastojen_lataus.jl")))

#= Mittauspöytäkirjan lukuarvot ja listat (mielellään vakioina const) =#

const MITTAUSPÖYTÄKIRJA = missing

#= Apufunktioita (lisää tarvitsemasi funktiot tähän) =#

"""
Laskee menetelmävirheen korjauksen jännitteelle.
"""
function korjattu_jännite(#= parametrit =#)
    missing # Lauseke tänne
end

"""
Laskee menetelmävirheen korjauksen virralle.
"""
function korjattu_virta(#= parametrit =#)
    missing # Lauseke tänne
end

#= Päärutiini (täytä laskut tänne) =#

"""
Päärutiini. Lisätkää laskut tänne.
"""
function main()
    missing
end

# Pääfunktion kutsu.
main()
