# Kuvakansio

Jos olet käyttänyt kirjaston `TAUFysOplabLaskupohjat` tarjoamaa vakiota
`TAUFysOplabLaskupohjat.KUVAKANSIO` oikein, kansion `laskupohjat` skriptien
tuottamien kuvien pitäisi tulostua tähän kansioon. Vakion käyttö tapahtuu
tuomalla kirjasto laskuskriptin alussa ohjelman käytettäväksi komennolla

	using TAUFysOplabLaskupohjat

tai

	import TAUFysOplabLaskupohjat

jonka jälkeen vakiota voidaan käyttää vastaavasti muodossa `KUVAKANSIO`
tai `TAUFysOplabLaskupohjat.KUVAKANSIO`.
