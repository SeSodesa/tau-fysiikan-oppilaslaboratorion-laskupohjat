"""
Tämä moduuli sisältää funktion `piirrä_mittapisteet_ja_sovitteet`,
sekä muutaman sen käyttämän apufunktion.
"""
module PiirräMittapisteetJaSovitteet

export piirrä_mittapisteet_ja_sovitteet

using Plots
import TAUFysOplabLaskupohjat:
	SELITTEEN_OLETUSSIJAINTI,
	OLETUSVÄRIPALETTI,
	OLETUSVÄRI,
	KUVAKANSIO,
	VIRHESYMBOLI

"""
Tallentaa kuvakansioon kuvan annetuista mittapisteistä ja niihin tehdyistä
sovitteista. Mittapisteet voi antaa joko x- ja y-matriisina, listoina listoja
tai yhtinä x- ja y-listana.

Kaikissa tapauksissa tiettyjä käyriä vastaavien x- ja y-koordinaattilistojen
kokojen tulee tietysti vastata toisiaan. Matriisien tapauksessa data tulee
sijoittaa sarakkeittain.
"""
function piirrä_mittapisteet_ja_sovitteet() end

"""
Matriisimetodi tästä funktiosta. Vaaka- ja pystydatojen oletetaan olevan
sarakemajorantteja matriiseja, eli mittasarjojen tulee olla niissä
sarakkeittain.
"""
function piirrä_mittapisteet_ja_sovitteet(
	kuvatiedoston_nimi::KuvanNimi,
	vaakadata::Vaakadata,
	pystydata::Pystydata,
	sovitteiden_parametrit::Parametrit,
	sovitteiden_muodot::SovitteidenMuodot,
	vaakaotsikko::Vaakaotsikko,
	pystyotsikko::Pystyotsikko,
	nimet_selitelaatikossa::NimetSelitelaatikossa;
	selitelaatikon_sijainti::Symbol = SELITTEEN_OLETUSSIJAINTI,
	väripaletti::Symbol = OLETUSVÄRIPALETTI,
	osituksen_osavälejä::Osavälejä = 10,
	virheindeksit::Virheindeksit = Dict{Int, Vector{Int}}(),
) where {
	KuvanNimi			  <: AbstractString,
	Vaakadata			  <: Matrix{<:Number},
	Pystydata			  <: Matrix{<:Number},
	Parametrit			  <: Vector{<:Vector{<:Number}},
	SovitteidenMuodot	  <: Vector{<:Function},
	Vaakaotsikko		  <: AbstractString,
	Pystyotsikko		  <: AbstractString,
	NimetSelitelaatikossa <: Vector{<:AbstractString},
	Osavälejä			  <: Integer,
	Virheindeksit		  <: Dict{<:Integer, <:Vector{<:Integer}},
}
	xkoko, ykoko = size(vaakadata), size(pystydata)
	if xkoko ≠ ykoko
		error("Vaaka- ja pystydatamatriisien tulee olla saman kokoisia…")
	end

	sarakkeita = xkoko[2]

	if length(nimet_selitelaatikossa) ≠ sarakkeita
		error(
			"Selitelaatikon otsikoita tulee olla yhtä monta kuin" *
			" datamatriiseissa on sarakkeita."
		)
	end

	if length(sovitteiden_parametrit) ≠ sarakkeita
		error(
			"Sovitteiden parametrilistoja tulee olla yhtä monta kuin" *
			" datamatriiseissa on sarakkeita."
		)
	end

	if length(sovitteiden_muodot) ≠ sarakkeita
		error(
			"Sovitteiden mallifunktioita tulee olla yhtä monta kuin" *
			" datamatriiseissa on sarakkeita."
		)
	end

	# Kuvan muodostus
	kuvan_polku = joinpath(KUVAKANSIO, kuvatiedoston_nimi)

	println(string("Piirretään kuvaa polkuun ", kuvan_polku, "…"))

	# Muodostetaan kuva
	kuva = Plots.plot(
		xlabel = vaakaotsikko,
		ylabel = pystyotsikko,
		legend = selitelaatikon_sijainti
	)

	# Lisätään sarakkeisiin 1, koska värigradientissa on ainakin 2 väriä
	värit = Plots.cgrad(väripaletti, sarakkeita + 1, categorical = true)

	# Lisätään data ja sovitteet kuvaan sarakkeittain
	for sarake ∈ 1 : sarakkeita
		Plots.scatter!(
			kuva,
			vaakadata[:, sarake],
			pystydata[:, sarake],
			label = nimet_selitelaatikossa[sarake],
			color = värit[sarake]
		)

		lisää_virheet_kuvaan(
			kuva,
			virheindeksit,
			vaakadata[:,sarake],
			pystydata[:,sarake],
			sarake,
			nimet_selitelaatikossa[sarake],
			värit
		)

		# Sovitteiden piirto
		vaakamin = minimum(vaakadata[:, sarake])
		vaakamax = maximum(vaakadata[:, sarake])
		ositus = (vaakamax - vaakamin) / osituksen_osavälejä

		sovitteen_vaakadata =
			vaakadata[:,sarake][begin] : ositus : vaakadata[:,sarake][end]

		sovitteen_pystydata = sovitteiden_muodot[sarake](
			sovitteen_vaakadata,
			sovitteiden_parametrit[sarake]
		)

		Plots.plot!(
			kuva,
			sovitteen_vaakadata,
			sovitteen_pystydata,
			linestyle=:dash,
			label = sovitteen_titteli(
				sovitteiden_muodot[sarake],
				nimet_selitelaatikossa[sarake]
			),
			color = värit[sarake]
		)
	end
	Plots.savefig(kuva, kuvan_polku)
end

"""
Matriisimetodi tästä funktiosta. Vaaka- ja pystydatojen oletetaan olevan
sarakemajorantteja matriiseja, eli mittasarjojen tulee olla niissä
sarakkeittain.

Ottaa vastaan vain yhden sovitteen muodon, eli kaikki sovitteet ovat
yhdenmuotoisia.
"""
function piirrä_mittapisteet_ja_sovitteet(
	kuvatiedoston_nimi::KuvanNimi,
	vaakadata::Vaakadata,
	pystydata::Pystydata,
	sovitteiden_parametrit::Parametrit,
	sovitteiden_muodot::SovitteidenMuodot,
	vaakaotsikko::Vaakaotsikko,
	pystyotsikko::Pystyotsikko,
	nimet_selitelaatikossa::NimetSelitelaatikossa;
	selitelaatikon_sijainti::Symbol = SELITTEEN_OLETUSSIJAINTI,
	väripaletti::Symbol = OLETUSVÄRIPALETTI,
	osituksen_osavälejä::Osavälejä = 10,
	virheindeksit::Virheindeksit = Dict{Int, Vector{Int}}(),
) where {
	KuvanNimi			  <: AbstractString,
	Vaakadata			  <: Matrix{<:Number},
	Pystydata			  <: Matrix{<:Number},
	Parametrit			  <: Vector{<:Vector{<:Number}},
	SovitteidenMuodot	  <: Function,
	Vaakaotsikko		  <: AbstractString,
	Pystyotsikko		  <: AbstractString,
	NimetSelitelaatikossa <: Vector{<:AbstractString},
	Osavälejä			  <: Integer,
	Virheindeksit		  <: Dict{<:Integer, <:Vector{<:Integer}},
}
	xkoko, ykoko = size(vaakadata), size(pystydata)
	if xkoko ≠ ykoko
		error("Vaaka- ja pystydatamatriisien tulee olla saman kokoisia…")
	end

	sarakkeita = xkoko[2]

	if length(nimet_selitelaatikossa) ≠ sarakkeita
		error(
			"Selitelaatikon otsikoita tulee olla yhtä monta kuin" *
			" datamatriiseissa on sarakkeita."
		)
	end

	if length(sovitteiden_parametrit) ≠ sarakkeita
		error(
			"Sovitteiden parametrilistoja tulee olla yhtä monta kuin" *
			" datamatriiseissa on sarakkeita."
		)
	end

	if length(sovitteiden_muodot) ≠ sarakkeita
		error(
			"Sovitteiden mallifunktioita tulee olla yhtä monta kuin" *
			" datamatriiseissa on sarakkeita."
		)
	end

	# Kuvan muodostus
	kuvan_polku = joinpath(KUVAKANSIO, kuvatiedoston_nimi)

	println(string("Piirretään kuvaa polkuun ", kuvan_polku, "…"))

	# Muodostetaan kuva
	kuva = Plots.plot(
		xlabel = vaakaotsikko,
		ylabel = pystyotsikko,
		legend = selitelaatikon_sijainti
	)

	# Lisätään sarakkeisiin 1, koska värigradientissa on ainakin 2 väriä
	värit = Plots.cgrad(väripaletti, sarakkeita + 1, categorical = true)

	# Lisätään data ja sovitteet kuvaan sarakkeittain
	for sarake ∈ 1 : sarakkeita
		Plots.scatter!(
			kuva,
			vaakadata[:, sarake],
			pystydata[:, sarake],
			label = nimet_selitelaatikossa[sarake],
			color = värit[sarake]
		)

		lisää_virheet_kuvaan(
			kuva,
			virheindeksit,
			vaakadata,
			pystydata,
			sarake,
			nimet_selitelaatikossa[sarake],
			värit
		)

		# Sovitteiden piirto
		vaakamin = minimum(vaakadata[:, sarake])
		vaakamax = maximum(vaakadata[:, sarake])
		ositus = (vaakamax - vaakamin) / osituksen_osavälejä

		sovitteen_vaakadata =
			vaakadata[:,sarake][begin] : ositus : vaakadata[:,sarake][end]

		sovitteen_pystydata = sovitteiden_muoto(
			sovitteen_vaakadata,
			sovitteiden_parametrit[sarake]
		)

		Plots.plot!(
			kuva,
			sovitteen_vaakadata,
			sovitteen_pystydata,
			linestyle=:dash,
			label = sovitteen_titteli(
				sovitteiden_muodot[indeksi],
				nimet_selitelaatikossa[indeksi]
			),
			color = värit[sarake]
		)
	end
	Plots.savefig(kuva, kuvan_polku)
end

"""
Vektorilistametodi tästä funktiosta. Vaaka- ja pystydatojen oletetaan olevan
listojen listoja, missä vastaavat vaaka- ja pystydatalistat ovat yhtä pitkiä.
"""
function piirrä_mittapisteet_ja_sovitteet(
	kuvatiedoston_nimi::KuvanNimi,
	vaakadata::Vaakadata,
	pystydata::Pystydata,
	sovitteiden_parametrit::Parametrit,
	sovitteiden_muodot::SovitteidenMuodot,
	vaakaotsikko::Vaakaotsikko,
	pystyotsikko::Pystyotsikko,
	nimet_selitelaatikossa::NimetSelitelaatikossa;
	selitelaatikon_sijainti::Symbol = SELITTEEN_OLETUSSIJAINTI,
	väripaletti::Symbol = OLETUSVÄRIPALETTI,
	osituksen_osavälejä::Osavälejä = 10,
	virheindeksit::Virheindeksit = Dict{Int, Vector{Int}}(),
) where {
	KuvanNimi			  <: AbstractString,
	Vaakadata			  <: Vector{<:Vector{<:Number}},
	Pystydata			  <: Vector{<:Vector{<:Number}},
	Parametrit			  <: Vector{<:Vector{<:Number}},
	SovitteidenMuodot	  <: Vector{<:Function},
	Vaakaotsikko		  <: AbstractString,
	Pystyotsikko		  <: AbstractString,
	NimetSelitelaatikossa <: Vector{<:AbstractString},
	Osavälejä			  <: Integer,
	Virheindeksit		  <: Dict{<:Integer, <:Vector{<:Integer}},
}
	xkoko, ykoko = length(vaakadata), length(pystydata)
	if xkoko ≠ ykoko
		error(" Mittasarjojen vaaka- ja pystyvektoreita tulee olla yhtä monta…")
	end

	jonoja = xkoko

	if length(nimet_selitelaatikossa) ≠ jonoja
		error(
			"Selitelaatikon otsikoita tulee olla yhtä monta kuin" *
			" vaaka- ja pystyvektoreita…"
		)
	end

	if length(sovitteiden_parametrit) ≠ jonoja
		error(
			"Sovitteiden parametrilistoja tulee olla yhtä monta kuin" *
			" vaaka- ja pystyvektoreita…"
		)
	end

	if length(sovitteiden_muodot) ≠ jonoja
		error(
			"Sovitteiden mallifunktioita tulee olla yhtä monta kuin" *
			" datamatriiseissa on sarakkeita."
		)
	end

	# Kuvan muodostus
	kuvan_polku = joinpath(KUVAKANSIO, kuvatiedoston_nimi)

	println(string("Piirretään kuvaa polkuun ", kuvan_polku, "…"))

	# Muodostetaan kuva
	kuva = Plots.plot(
		xlabel = vaakaotsikko,
		ylabel = pystyotsikko,
		legend = selitelaatikon_sijainti
	)

	# Lisätään sarakkeisiin 1, koska värigradientissa on ainakin 2 väriä
	värit = Plots.cgrad(väripaletti, jonoja + 1, categorical = true)

	# Lisätään data ja sovitteet kuvaan sarakkeittain
	for indeksi ∈ 1 : jonoja
		if length(vaakadata[indeksi]) ≠ length(pystydata[indeksi])
			error("Vaaka- ja pystyvektorit $indeksi eivät ole yhtä pitkät…")
		end

		Plots.scatter!(
			kuva,
			vaakadata[indeksi],
			pystydata[indeksi],
			label = nimet_selitelaatikossa[indeksi],
			color = värit[indeksi]
		)

		lisää_virheet_kuvaan(
			kuva,
			virheindeksit,
			vaakadata[indeksi],
			pystydata[indeksi],
			indeksi,
			nimet_selitelaatikossa[indeksi],
			värit
		)

		# Sovitteen piirto

		vaakamin = minimum(vaakadata[indeksi])
		vaakamax = maximum(vaakadata[indeksi])
		ositus = (vaakamax - vaakamin) / osituksen_osavälejä

		sovitteen_vaakadata =
			vaakadata[indeksi][begin] : ositus : vaakadata[indeksi][end]

		sovitteen_pystydata = sovitteiden_muodot[indeksi](
			sovitteen_vaakadata,
			sovitteiden_parametrit[indeksi]
		)

		Plots.plot!(
			kuva,
			sovitteen_vaakadata,
			sovitteen_pystydata,
			linestyle=:dash,
			label = sovitteen_titteli(
				sovitteiden_muodot[indeksi],
				nimet_selitelaatikossa[indeksi]
			),
			color = värit[indeksi]
		)
	end
	Plots.savefig(kuva, kuvan_polku)
end

"""
Vektorilistametodi tästä funktiosta. Vaaka- ja pystydatojen oletetaan olevan
listojen listoja, missä vastaavat vaaka- ja pystydatalistat ovat yhtä pitkiä.

Tarvitsee vain yhden sovitteen muodon ja piirtää kaikki sovitteet
yhdenmuotoisina.
"""
function piirrä_mittapisteet_ja_sovitteet(
	kuvatiedoston_nimi::KuvanNimi,
	vaakadata::Vaakadata,
	pystydata::Pystydata,
	sovitteiden_parametrit::Parametrit,
	sovitteiden_muodot::SovitteidenMuodot,
	vaakaotsikko::Vaakaotsikko,
	pystyotsikko::Pystyotsikko,
	nimet_selitelaatikossa::NimetSelitelaatikossa;
	selitelaatikon_sijainti::Symbol = SELITTEEN_OLETUSSIJAINTI,
	väripaletti::Symbol = OLETUSVÄRIPALETTI,
	osituksen_osavälejä::Osavälejä = 10,
	virheindeksit::Virheindeksit = Dict{Int, Vector{Int}}(),
) where {
	KuvanNimi			  <: AbstractString,
	Vaakadata			  <: Vector{<:Vector{<:Number}},
	Pystydata			  <: Vector{<:Vector{<:Number}},
	Parametrit			  <: Vector{<:Vector{<:Number}},
	SovitteidenMuodot	  <: Function,
	Vaakaotsikko		  <: AbstractString,
	Pystyotsikko		  <: AbstractString,
	NimetSelitelaatikossa <: Vector{<:AbstractString},
	Osavälejä			  <: Integer,
	Virheindeksit		  <: Dict{<:Integer, <:Vector{<:Integer}},
}
	xkoko, ykoko = length(vaakadata), length(pystydata)
	if xkoko ≠ ykoko
		error(" Mittasarjojen vaaka- ja pystyvektoreita tulee olla yhtä monta…")
	end

	jonoja = xkoko

	if length(nimet_selitelaatikossa) ≠ jonoja
		error(
			"Selitelaatikon otsikoita tulee olla yhtä monta kuin" *
			" vaaka- ja pystyvektoreita…"
		)
	end

	if length(sovitteiden_parametrit) ≠ jonoja
		error(
			"Sovitteiden parametrilistoja tulee olla yhtä monta kuin" *
			" vaaka- ja pystyvektoreita…"
		)
	end

	if length(sovitteiden_muodot) ≠ jonoja
		error(
			"Sovitteiden mallifunktioita tulee olla yhtä monta kuin" *
			" datamatriiseissa on sarakkeita."
		)
	end

	# Kuvan muodostus
	kuvan_polku = joinpath(KUVAKANSIO, kuvatiedoston_nimi)

	println(string("Piirretään kuvaa polkuun ", kuvan_polku, "…"))

	# Muodostetaan kuva
	kuva = Plots.plot(
		xlabel = vaakaotsikko,
		ylabel = pystyotsikko,
		legend = selitelaatikon_sijainti
	)

	# Lisätään sarakkeisiin 1, koska värigradientissa on ainakin 2 väriä
	värit = Plots.cgrad(väripaletti, jonoja + 1, categorical = true)

	# Lisätään data ja sovitteet kuvaan sarakkeittain
	for indeksi ∈ 1 : jonoja
		if length(vaakadata[indeksi]) ≠ length(pystydata[indeksi])
			error("Vaaka- ja pystyvektorit $indeksi eivät ole yhtä pitkät…")
		end

		Plots.scatter!(
			kuva,
			vaakadata[indeksi],
			pystydata[indeksi],
			label = nimet_selitelaatikossa[indeksi],
			color = värit[indeksi]
		)

		lisää_virheet_kuvaan(
			kuva,
			virheindeksit,
			vaakadata[indeksi],
			pystydata[indeksi],
			indeksi,
			nimet_selitelaatikossa[indeksi],
			värit
		)

		# Sovitteen piirto

		vaakamin = minimum(vaakadata[indeksi])
		vaakamax = maximum(vaakadata[indeksi])
		ositus = (vaakamax - vaakamin) / osituksen_osavälejä

		sovitteen_vaakadata =
			vaakadata[indeksi][begin] : ositus : vaakadata[indeksi][end]

		sovitteen_pystydata = sovitteiden_muoto(
			sovitteen_vaakadata,
			sovitteiden_parametrit[indeksi]
		)

		Plots.plot!(
			kuva,
			sovitteen_vaakadata,
			sovitteen_pystydata,
			linestyle=:dash,
			label = sovitteen_titteli(
				sovitteiden_muodot[indeksi],
				nimet_selitelaatikossa[indeksi]
			),
			color = värit[indeksi]
		)
	end
	Plots.savefig(kuva, kuvan_polku)
end

"""
Tämä funktion metodi olettaa saavansa tasan yhden mittaussarjan
vaaka- ja pystykoordinaatit sarakevektoreissa.
"""
function piirrä_mittapisteet_ja_sovitteet(
	kuvatiedoston_nimi::KuvanNimi,
	vaakadata::Vaakadata,
	pystydata::Pystydata,
	sovitteiden_parametrit::Parametrit,
	sovitteiden_muodot::SovitteidenMuodot,
	vaakaotsikko::Vaakaotsikko,
	pystyotsikko::Pystyotsikko,
	nimet_selitelaatikossa::NimetSelitelaatikossa;
	selitelaatikon_sijainti::Symbol = SELITTEEN_OLETUSSIJAINTI,
	väripaletti::Symbol = OLETUSVÄRIPALETTI,
	osituksen_osavälejä::Osavälejä = 10,
	virheindeksit::Virheindeksit = Int[],
) where {
	KuvanNimi			  <: AbstractString,
	Vaakadata			  <: Vector{<:Number},
	Pystydata			  <: Vector{<:Number},
	Parametrit			  <: Vector{<:Number},
	SovitteidenMuodot	  <: Function,
	Vaakaotsikko		  <: AbstractString,
	Pystyotsikko		  <: AbstractString,
	NimetSelitelaatikossa <: AbstractString,
	Osavälejä			  <: Integer,
	Virheindeksit		  <: Vector{<:Integer},
}
	xkoko, ykoko = length(vaakadata), length(pystydata)
	if xkoko ≠ ykoko
		error("Vaaka- ja pystyakseleiden tulee olla yhtä pitkät…")
	end

	# Kuvan muodostus
	kuvan_polku = joinpath(KUVAKANSIO, kuvatiedoston_nimi)

	println(string("Piirretään kuvaa polkuun ", kuvan_polku, "…"))

	# Muodostetaan kuva
	kuva = Plots.plot(
		xlabel = vaakaotsikko,
		ylabel = pystyotsikko,
		legend = selitelaatikon_sijainti
	)

	Plots.scatter!(
		kuva,
		vaakadata,
		pystydata,
		label = nimet_selitelaatikossa,
		color = väripaletti
	)

	if ! isempty(virheindeksit)
		if (
			minimum(virheindeksit) ≥ 1 &&
			maximum(virheindeksit) ≤ length(vaakadata)
		)
			Plots.scatter!(
				kuva,
				vaakadata[virheindeksit],
				pystydata[virheindeksit],
				label = "Karkea virhe($selitelaatikon_otsikko)",
				markershape = VIRHESYMBOLI,
				color = väripaletti,
			)
		else
			error(
				"Mittausdatan virheitä piirrettäessä törmättiin" *
				" liian pieneen tai suureen indeksiin…"
			)
		end
	end

	vaakamin = minimum(vaakadata)
	vaakamax = maximum(vaakadata)
	ositus = (vaakamax - vaakamin) / osituksen_osavälejä

	sovitteen_vaakadata =
		vaakadata[begin] : ositus : vaakadata[end]

	sovitteen_pystydata = sovitteiden_muodot(
		sovitteen_vaakadata,
		sovitteiden_parametrit
	)

	Plots.plot!(
		kuva,
		sovitteen_vaakadata,
		sovitteen_pystydata,
		linestyle=:dash,
		label = sovitteen_titteli(sovitteiden_muodot, nimet_selitelaatikossa),
		color = väripaletti
	)
	Plots.savefig(kuva, kuvan_polku)
end

"""
Muuntaa annetun funktiosymbolin sovitteen titteliksi selitelaatikossa.
"""
function sovitteen_titteli(
	sovitteen_muoto::SovitteenMuoto,
	jonon_nimi::JononNimi,
) where {
	SovitteenMuoto <: Function,
	JononNimi	   <: AbstractString,
}
	string(
		uppercasefirst(String(Symbol(sovitteen_muoto))), "(", jonon_nimi, ")"
	)
end

"""
Jos annetussa sanakirjassa on annetuss indeksissa virheindeksejä, lisää niiden
kuvaaja annettuun kuvaan.
"""
function lisää_virheet_kuvaan(
	kuva::Plots.Plot,
	virheindeksit::Virheindeksit,
	vaakadata::Vaakadata,
	pystydata::Pystydata,
	indeksi::Indeksi,
	sarjan_nimi::SarjanNimi,
	väri::Väri;
	virhesymboli::Symbol = VIRHESYMBOLI,
) where {
	Virheindeksit  <: Dict{<:Integer, <: Vector{<:Integer}},
	Vaakadata	   <: Vector{<:Number},
	Pystydata	   <: Vector{<:Number},
	Indeksi		   <: Integer,
	SarjanNimi	   <: AbstractString,
	Väri		   <: PlotUtils.CategoricalColorGradient,
}
	# Lisätään virheet vaihtoehtoisesti kuvaan
	virheindeksit = get(virheindeksit, indeksi, [])

	if ! isempty(virheindeksit)
		if (
			minimum(virheindeksit) ≥ 1 &&
			maximum(virheindeksit) ≤ length(vaakadata) &&
			maximum(virheindeksit) ≤ length(pystydata)
		)
			Plots.scatter!(
				kuva,
				vaakadata[virheindeksit],
				pystydata[virheindeksit],
				label = "Karkea virhe($sarjan_nimi)",
				marker = virhesymboli,
				color = väri
			)
		else
			error(
				"Sarakkeen $sarake virheitä piirrettäessä törmättiin" *
				" liian pieneen tai suureen indeksiin…"
			)
		end
	end
end
end
