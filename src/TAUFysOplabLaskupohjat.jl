"""
Projektin päämoduuli.

TODO: Tänne voisi lisätä vakioita ja apufunktioita sun muuta.
"""
module TAUFysOplabLaskupohjat

export
	piirrä_mittapisteet_ja_sovitteet,
	KUVAKANSIO,
	𝑔,
	otsikko

using
	Plots

"""
Polku projektin juuren alla olevaan kuvakansioon.
"""
const KUVAKANSIO = normpath(joinpath(@__DIR__, "..", "kuvat"))

"""
Piirtofunktiot käyttävät tätä oletettuna nimettynä argumenttina.
"""
const OLETUSVÄRIPALETTI = :lightrainbow

"""
Piirtofunktiot jotka tarvitsevat vain yhden värin käyttävät tätä väriä
oletuksena.
"""
const OLETUSVÄRI = :steelblue

"""
Selitelaatikon oletussijainti kuvissa.
"""
const SELITTEEN_OLETUSSIJAINTI = :bottomright

"""
Tätä muotoa käytetään virheellisten mittapisteiden muotona.
"""
const VIRHESYMBOLI = :xcross

"""
Painovoiman aiheuttama kiihtyvyys Tampereella, leveyspiirillä 61.49911° ja
korkeudella ∼100 m. Huomaa, että symboli on UTF-8-merkistön kursiivi 𝑔.
"""
const 𝑔 = 9.82003

# ------------------------------- Funktioita ----------------------------------

"""
Tulostaa annetun otsikon kahden tasapitkän viivan keskelle.
"""
function otsikko(
	otsikko::AbstractString;
	rivin_leveys=80,
	viivamerkki = '-'
)
	otsikko = strip(otsikko)
	otsikon_pituus = lastindex(otsikko)
	otsikon_tila = otsikon_pituus + 2 * length(" ")
	viivamerkkejä = div(rivin_leveys - otsikon_tila, 2)
	println(
		'\n',
		viivamerkki^viivamerkkejä,
		" $otsikko ",
		viivamerkki^viivamerkkejä,
		'\n'
	)
end

include("PiirräMittapisteetJaSovitteet.jl")
using .PiirräMittapisteetJaSovitteet

end # TAUFysOplabLaskupohjat
