println("Varmistetaan, että nykyinen projekti on aktiivinen…")
const YMPÄRISTÖN_KANSIO = normpath(joinpath(@__DIR__, ".."))
if Base.active_project() ≠ YMPÄRISTÖN_KANSIO
    import Pkg;
    Pkg.activate(normpath(joinpath(@__DIR__, "..")));
end
println("Varmistetaan riippuvuuksien saatavuus…")
Pkg.instantiate()

# GR ei odota koodia ajettavan näytöllisellä laitteella
ENV["GKSwstype"] = "100"
