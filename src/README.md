# Projektin moduulit

Kansio sisältää projektiin liittyvät moduulit ja aputiedostot. Näihin ei
välttämättä kannata koskea, jos ei halua rikkoa laskupohjien toimintaa.
